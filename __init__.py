# -*- coding: utf-8 -*-
#
# Title:   mstat_front-end_api
# Author:  Mats Blomdahl
# Version: 2013-09-04

u"""mstat_front-end_api module

Usage:
    Invoke URLs at ``/<admin|user>/api/<cmd>[/<cmd>]`` as REST or Ext Direct endpoints.
"""
