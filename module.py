# -*- coding: utf-8 -*-
#
# Title:   mstat_front-end_api.module.app
# Author:  Mats Blomdahl
# Version: 2014-04-02

from __future__ import absolute_import

import logging
import webapp2
import os
import jinja2
import jinja2.ext as jinja2_extensions

from webapp2_extras import sessions

import ecore
import ecore.constants as ecore_constants

import mstat_constants as constants
from mstat_default.settings import CONFIG
from mstat_default.core import bq_orm

_TEMPLATING_DIR = '%s/../templates' % os.path.dirname(__file__)

logging.debug('__file__: %s / templating_dir: %s' % (__file__, _TEMPLATING_DIR))

# update/override with module specific settings
CONFIG.update({
    # Templating Engine
    'jinja_environment': jinja2.Environment(
        loader=jinja2.FileSystemLoader(_TEMPLATING_DIR),
        extensions=[jinja2_extensions.WithExtension]
    ),
    # Session Management
    'session_dict': sessions.SessionDict({}, data=None, new=False),
    'webapp2_extras.sessions': {
        'secret_key': constants.COOKIE_ENCRYPTION_KEY,
        'cookie_name': constants.COOKIE_NAME,
        'cookie_args': {
            'path': ecore_constants.DEFAULT_COOKIE_PATH,
            'max_age': ecore_constants.DEFAULT_SESSION_EXPIRATION
        }
    }
})


def _handle_400(request, response, exception):
    logging.exception(exception)
    response.content_type = 'text/plain'
    response.write(
        'The server could not comply with the request since it is either malformed or otherwise incorrect.')
    response.set_status(400)


def _handle_404(request, response, exception):
    logging.exception(exception)
    response.content_type = 'text/plain'
    response.write('Oops! I could swear this page was here!')
    response.set_status(404)


def _handle_500(request, response, exception):
    logging.exception(exception)
    response.content_type = 'text/plain'
    response.write('A server error occurred!')
    response.set_status(500)


app = webapp2.WSGIApplication(routes=[
    # Handler starting the backend service.
    webapp2.Route('/_ah/start',
                  handler='ecore.handler.BackendStartup'),

    # TODO(mats.blomdahl@gmail.com): Re-implement/deprecate.
    # Handler for receiving incoming mail
    # webapp2.Route('/_ah/mail/<email:(.+)>',
    #               handler='mstat_parser.EmailClient',
    #               methods=['POST']),

    # TODO(mats.blomdahl@gmail.com): Re-implement/deprecate.
    # (r'/public/api/(\w+)', 'mstat_front-end_api.public.ApiInterface'),
    # (r'/public/api/(\w+)/(.+)', 'mstat_front-end_api.public.ApiInterface'),

    # Handlers deployed within the `User API`.
    webapp2.Route('/user/api/<:\w+>',
                  handler='mstat_front-end_api.user.ApiInterface',
                  methods=['GET', 'POST']),

    # Handlers deployed within the `User API`.
    webapp2.Route('/user/api/<:\w+>/<:.+>',
                  handler='mstat_front-end_api.user.ApiInterface',
                  methods=['GET', 'POST']),

    # Handlers deployed within the `Admin API`.
    webapp2.Route('/admin/api/<:\w+>',
                  handler='mstat_front-end_api.admin.ApiInterface',
                  methods=['GET', 'POST']),

    # Handlers deployed within the `Admin API`.
    webapp2.Route('/admin/api/<:\w+>/<:.+>',
                  handler='mstat_front-end_api.admin.ApiInterface',
                  methods=['GET', 'POST']),

    # TODO(mats.blomdahl@gmail.com): Re-eval/deprecate.
    # webapp2.Route('/sys/upload',
    #               handler='mstat_front-end_api.sys.UploadHandler',
    #               methods=['POST']),

    # TODO(mats.blomdahl@gmail.com): Re-eval/deprecate.
    # webapp2.Route('/sys/serve/<:.*>',
    #               handler='mstat_front-end_api.sys.ServeHandler',
    #               methods=['GET']),

    # TODO(mats.blomdahl@gmail.com): Re-eval/deprecate.
    # webapp2.Route('/sys/datastore_to_bigquery/<:(run|abort|status|dumps)>',
    #               handler='mstat_front-end_api.sys.DatastoreToBigQuery',
    #               methods=['GET']),

    # TODO(mats.blomdahl@gmail.com): Re-eval/deprecate.
    # webapp2.Route('/sys/bigquery/<:(tabledata|tables|datasets|jobs)>/<:\w+>',
    #               handler='mstat_front-end_api.sys.bigquery.ApiInterface',
    #               methods=['GET', 'POST'])

], config=CONFIG, debug=True)

app.error_handlers[400] = _handle_400
app.error_handlers[404] = _handle_404
app.error_handlers[500] = _handle_500

bq_orm.init_app(app)

def main():
    u"""Starts the webapp2 framework."""

    app.run()


if __name__ == '__main__':
    main()

