import functools

import ecore.utils as ecore_utils
import ecore.model.user.errors as ecore_user_model_errors
import ecore.constants as ecore_constants

from mstat_generic_handler.common_ext_direct_interface import CommonExtDirectInterface

def require_access_key(function):
    def wrapped_f(self, command, *args, **kwargs):
        api_access_key = command.payload.get('api_access_key')

        errors = {}
        subscription_entry = None

        if ecore_utils.assert_presence_and_basestring(api_access_key):
            try:
                subscription, user = CommonExtDirectInterface._resolve_api_access_key(api_access_key.strip())

            except (ecore_user_model_errors.ApiAccessKeyInvalidError, user_model_errors.SubscriptionNotFoundError,
                    ecore_user_model_errors.UserNotFoundError) as e:
                logging.error("%s %s" % (type(e), e))
                errors = {'INVALID API ACCESS KEY': 'Malformed/invalid \'apiAccessKey\' argument (%r).' % api_access_key}

            except ecore_user_model_errors.UserSuspendedError as e:
                errors = {'USER ACCOUNT SUSPENDED': 'Cannot process query due to suspension. %s' % e.message}

            except ecore_user_model_errors.UserDeletedError as e:
                errors = {'USER ACCOUNT DELETED': 'Cannot process query due to deletion. %s' % e.message}

            except ecore_user_model_errors.ParentAccountDeletedError as e:
                errors = {'PARENT ACCOUNT DELETED': 'Cannot process query due to deletion. %s' % e.message}

            except ecore_user_model_errors.ParentAccountSuspendedError as e:
                errors = {'PARENT ACCOUNT SUSPENDED': 'Cannot process query due to suspension. %s' % e.message}

            except user_model_errors.SubscriptionSuspendedError as e:
                errors = {'SUBSCRIPTION SUSPENDED': 'Cannot process query due to suspension. %s' % e.message}

            except user_model_errors.SubscriptionDeletedError as e:
                errors = {'SUBSCRIPTION DELETED': 'Cannot process query due to deletion. %s' % e.message}
        else:
            errors = {'API ACCESS KEY MISSING': 'Required \'apiAccessKey\' argument could not be resolved.'}

        if errors:
            return command.set_response_data({
                'status_code': ecore_constants.HTTP_FORBIDDEN.get_status_code(),
                'errors': errors,
            })


        return function(self, command, user, subscription, *args, **kwargs)
    return functools.update_wrapper(wrapped_f, function)
