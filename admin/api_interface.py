# -*- coding: utf-8 -*-
#
# Title:   mstat_front-end_api.admin.ApiInterface
# Author:  Mats Blomdahl
# Version: 2013-09-21

import mstat_generic_handler as generic_handler

import ext_direct_interface


class ApiInterface(generic_handler.CommonApiInterface):
    """Misc. handlers specific to the `Admin API` (deployed on the ``/admin/api`` path).

    Security: All handlers are secured by App Engine's built-in authentication (`/mstat_front-end_api/handlers.yaml`
        config).
    """

    _ROUTE = '/admin/api'
    _EXT_DIRECT_HANDLERS = ext_direct_interface.AdminCmd
    _INTERFACE_CLASS = 'admin'
    _INTERFACE_FRIENDLY_NAME = 'admin.ApiInterface'
    _DEVMODE = True

    # Interface: /admin/api/login
    def login(self, command):
        u"""Performs admin login.

        Args:
            command: A ``Command`` RPC wrapper.

        Returns:
            A call to the ``admin_login`` handler.
        """

        ext_direct_handler = self._EXT_DIRECT_HANDLERS()
        return ext_direct_handler.admin_login(command, **command.payload)

    # Interface: /admin/api/memcache/<cmd>
    def memcache(self, command):
        u"""Temp dev handler, providing low-level interface to the GAE Memcache Service.

        Args:
            command: A ``Command`` RPC wrapper.

        Returns:
            A call to the ``memcache_cmd`` handler.
        """

        ext_direct_handler = self._EXT_DIRECT_HANDLERS()
        return ext_direct_handler.memcache_cmd(command, **command.payload)

    # Interface: /admin/api/about_user
    def about_user(self, command):
        u"""Retrieves user session detail.

        Args:
            command: A ``Command`` RPC wrapper.

        Returns:
            A call to the ``about_user`` handler.
        """

        ext_direct_handler = self._EXT_DIRECT_HANDLERS()
        return ext_direct_handler.about_user(command, **command.payload)

    # Interface: /admin/api/add_or_edit_subscription
    def add_or_edit_subscription(self, command):
        u"""Adds or edits a ``Subscription`` entity within a ``SubscriberAccount``.

        Args:
            command: A ``Command`` RPC wrapper.

        Returns:
            A call to the ``add_or_edit_subscription`` handler.
        """

        ext_direct_handler = self._EXT_DIRECT_HANDLERS()
        return ext_direct_handler.add_or_edit_subscription(command, **command.payload)

    # Interface: /admin/api/suspend_subscription
    def suspend_subscription(self, command):
        u"""Updates a ``Subscription`` config entity within a ``SubscriberAccount`` to being `suspended`.

        Args:
            command: A ``Command`` RPC wrapper.

        Returns:
            A call to the ``suspend_subscription`` handler.
        """

        ext_direct_handler = self._EXT_DIRECT_HANDLERS()
        return ext_direct_handler.suspend_subscription(command, **command.payload)

    # Interface: /admin/api/unsuspend_subscription
    def unsuspend_subscription(self, command):
        u"""Updates a ``Subscription`` config entity within a ``SubscriberAccount`` to _not_ being `suspended`.

        Args:
            command: A ``Command`` RPC wrapper.

        Returns:
            A call to the ``unsuspend_subscription`` handler.
        """

        ext_direct_handler = self._EXT_DIRECT_HANDLERS()
        return ext_direct_handler.unsuspend_subscription(command, **command.payload)

    # Interface: /admin/api/delete_subscription
    def delete_subscription(self, command):
        u"""Marks a ``Subscription`` config entity within a ``SubscriberAccount`` as having been `deleted`.

        Args:
            command: A ``Command`` RPC wrapper.

        Returns:
            A call to the ``delete_subscription`` handler.
        """

        ext_direct_handler = self._EXT_DIRECT_HANDLERS()
        return ext_direct_handler.delete_subscription(command, **command.payload)

    # Interface: /admin/api/add_or_edit_user_account
    def add_or_edit_user_account(self, command):
        u"""Adds or edits a ``UserAccount`` entity within a ``SubscriberAccount``.

        Args:
            command: A ``Command`` RPC wrapper.

        Returns:
            A call to the ``add_or_edit_user_account`` handler.
        """

        ext_direct_handler = self._EXT_DIRECT_HANDLERS()
        return ext_direct_handler.add_or_edit_user_account(command, **command.payload)

    # Interface: /admin/api/suspend_user_account
    def suspend_user_account(self, command):
        u"""Updates a ``UserAccount`` entity within a ``SubscriberAccount`` to being `suspended`.

        Args:
            command: A ``Command`` RPC wrapper.

        Returns:
            A call to the ``suspend_user_account`` handler.
        """

        ext_direct_handler = self._EXT_DIRECT_HANDLERS()
        return ext_direct_handler.suspend_user_account(command, **command.payload)

    # Interface: /admin/api/unsuspend_user_account
    def unsuspend_user_account(self, command):
        u"""Updates a ``UserAccount`` entity within a ``SubscriberAccount`` to _not_ being `suspended`.

        Args:
            command: A ``Command`` RPC wrapper.

        Returns:
            A call to the ``unsuspend_user_account`` handler.
        """

        ext_direct_handler = self._EXT_DIRECT_HANDLERS()
        return ext_direct_handler.unsuspend_user_account(command, **command.payload)

    # Interface: /admin/api/delete_user_account
    def delete_user_account(self, command):
        u"""Marks a ``UserAccount`` entity within a ``SubscriberAccount`` as having been `deleted`.

        Args:
            command: A ``Command`` RPC wrapper.

        Returns:
            A call to the ``delete_user_account`` handler.
        """

        ext_direct_handler = self._EXT_DIRECT_HANDLERS()
        return ext_direct_handler.delete_user_account(command, **command.payload)

    # Interface: /admin/api/add_or_edit_subscriber_account
    def add_or_edit_subscriber_account(self, command):
        u"""Adds or edits a ``SubscriberAccount`` entity.

        Args:
            command: A ``Command`` RPC wrapper.

        Returns:
            A call to the ``add_or_edit_subscriber_account`` handler.
        """

        ext_direct_handler = self._EXT_DIRECT_HANDLERS()
        return ext_direct_handler.add_or_edit_subscriber_account(command, **command.payload)

    # Interface: /admin/api/suspend_subscriber_account
    def suspend_subscriber_account(self, command):
        u"""Updates a ``SubscriberAccount`` entity, along with any descendant ``UserAccount`` or ``Subscription``
            entities, to being `suspended`.

        Args:
            command: A ``Command`` RPC wrapper.

        Returns:
            A call to the ``suspend_subscriber_account`` handler.
        """

        ext_direct_handler = self._EXT_DIRECT_HANDLERS()
        return ext_direct_handler.suspend_subscriber_account(command, **command.payload)

    # Interface: /admin/api/unsuspend_subscriber_account
    def unsuspend_subscriber_account(self, command):
        u"""Updates a ``SubscriberAccount`` entity, along with any descendant ``UserAccount`` or ``Subscription``
            entities, to _not_ being `suspended`.

        Args:
            command: A ``Command`` RPC wrapper.

        Returns:
            A call to the ``unsuspend_subscriber_account`` handler.
        """

        ext_direct_handler = self._EXT_DIRECT_HANDLERS()
        return ext_direct_handler.unsuspend_subscriber_account(command, **command.payload)

    # Interface: /admin/api/delete_subscriber_account
    def delete_subscriber_account(self, command):
        u"""Marks a ``SubscriberAccount`` entity, along with any descendant ``UserAccount`` or ``Subscription``
            entities, as having been `deleted`.

        Args:
            command: A ``Command`` RPC wrapper.

        Returns:
            A call to the ``delete_subscriber_account`` handler.
        """

        ext_direct_handler = self._EXT_DIRECT_HANDLERS()
        return ext_direct_handler.delete_subscriber_account(command, **command.payload)
