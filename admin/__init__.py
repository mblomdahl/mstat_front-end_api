# -*- coding: utf-8 -*-
#
# Title:   mstat_front-end_api.admin
# Author:  Mats Blomdahl
# Version: 2012-10-02

u"""mstat_front-end_api.admin module

Usage:
    Invoke handlers deployed on the ``/admin/api`` as REST or Ext Direct endpoints.
"""

from api_interface import ApiInterface
