# -*- coding: utf-8 -*-
#
# Title:   mstat_front-end_api.admin.AdminCmd
# Author:  Mats Blomdahl
# Version: 2013-09-24

import logging
import datetime
import pprint
import copy

from google.appengine.api import memcache
from google.appengine.ext import ndb

import ecore.formatting as formatting

import ecore.constants as ecore_constants

import ecore.model.generic as ecore_generic_model
import ecore.model.user as ecore_user_model

import mstat_constants as constants

import mstat_model.transactions as transaction_model
import mstat_model.user as user_model
import mstat_model.user.errors as user_model_errors

import mstat_generic_handler as generic_handler
import mstat_generic_handler.errors as errors


_DEBUG = False

_SESSION_INVALID_ERROR_MSG = 'Cannot process request without a valid and authenticated session.'


class AdminCmd(generic_handler.CommonExtDirectInterface):
    u"""Ext Direct endpoints/handlers specific to the `Admin API` (deployed on the ``/admin/api`` path).

    Security: All handlers are secured by App Engine's built-in authentication (`/mstat_front-end_api/handlers.yaml`
        config).
    """

    _ACCESSIBLE_TEMPLATES = {
        'admin_activation_email': u"Välkommen till Mäklarstatistiks informationslager/affärsystem!",
        'subscriber_user_activation_email': u"Välkommen till Mäklarstatistiks informationstjänster!",
        'subscription_notification_email': u"Nytt data-abonnemang för Mäklarstatistiks informationstjänster!",
        'safe_password_update_email': u"Lösenordsåterställning för Mäklarstatistiks informationstjänster"
    }
    _ACCESSIBLE_KINDS = {'SystemAdminAccount': user_model.SystemAdminAccount,
                         'SubscriberAccount': user_model.SubscriberAccount,
                         'UserAccount': user_model.UserAccount,
                         'Subscription': user_model.Subscription,
                         'TransactionCompositeEntry': transaction_model.TransactionCompositeEntry,
                         'SfdRawDataEntry': transaction_model.SfdRawDataEntry,
                         'LfRawDataEntry': transaction_model.LfRawDataEntry,
                         'SfRawDataEntry': transaction_model.SfRawDataEntry,
                         'ScbRawDataEntry': transaction_model.ScbRawDataEntry,
                         'CapitexRawDataEmail': transaction_model.CapitexRawDataEmail}
    _TO_JSON_DEFAULT_CFG = {'resolve_datastore_refs': 1,
                            'flatten_datastore_refs': True}

    def memcache_cmd(self, command):
        u"""Temp dev handler, providing low-level interface to the GAE Memcache Service.

        Args:
            command: A ``Command`` RPC wrapper.

        Returns:
            The RPC wrapper, configured with the handler's response.
        """

        if command.cmd == 'get_stats':
            return command.set_response_data({'success': True,
                                              'data': memcache.get_stats()})
        elif command.cmd == 'flush_all':
            return command.set_response_data({'success': True,
                                              'data': memcache.flush_all()})
        else:
            return command.set_response_data({'status_code': ecore_constants.HTTP_NOT_FOUND.get_status_code(),
                                              'errors': ecore_constants.HTTP_NOT_FOUND.get_description()})

    def about_user(self, command, user_account=None, user_email=None, username=None):
        u"""Retrieves user session detail.

        Args:
            command: A ``Command`` RPC wrapper.
            user_account: A urlsafe ``UserAccount`` key.
            user_email: An email address corresponding to one or more ``UserAccount`` entities.
            username: A username corresponding to one or more ``UserAccount`` entities.

        Returns:
            The RPC wrapper, configured with the handler's response.
        """

        assert user_account or user_email or username

        user_account_entities = []
        kind_cls = user_model.UserAccount

        if user_account:
            user_account_entities.append(ndb.Key(urlsafe=user_account).get())

        elif user_email:
            user_account_entities = kind_cls.query(kind_cls.user_email == user_email).fetch(20, batch_size=20)

        else:  # Implicit ``elif username:``.
            user_account_entities = kind_cls.query(kind_cls.username == username).fetch(20, batch_size=20)

        response_data = []
        activity_kind_cls = ecore_user_model.UserActivity
        for account_entity in user_account_entities:
            activity_query = activity_kind_cls.query(ancestor=account_entity.key).order(-activity_kind_cls.sys_created)
            user_activity_history = map(self._to_json, activity_query.fetch(3))
            current_user_activity_json = user_activity_history.pop(0) if any(user_activity_history) else None

            response_data.append({'user_account': self._to_json(account_entity),
                                  'organization_account': self._to_json(account_entity.key.parent().get()),
                                  'session_trace': {
                                      'current_session': current_user_activity_json,
                                      'session_history': user_activity_history
                                  }})

        return command.set_response_data({'success': True,
                                          'data': response_data})

    def add_or_edit_subscription(self, command, subscription=None, ancestor=None, quota=None, friendly_name=None,
                                 description=None, subscription_type=None, start_date=None, end_date=None,
                                 update_frequency=None, content_restrictions=None):
        u"""Adds or edits a ``Subscription`` entity within a ``SubscriberAccount``.

        Args:
            command: A ``Command`` RPC wrapper.
            subscription: A urlsafe ``Subscription`` key.
            ancestor: A urlsafe parent ``SubscriberAccount`` key (required).
            quota: Usage quota limit (required).
            friendly_name: A title or `friendly name` for the subscription (required).
            description: Optional description.
            subscription_type: Type of subscription; valid inputs are ``data_api`` or ``widget_service`` (required).
            start_date: Start date for the subscription availability (required).
            end_date: Optional end date, for when the subscription will become inaccessible.
            update_frequency: Data update frequency; monthly, weekly or daily (required).
            content_restrictions: A dict-like value containing the keys ``datastore_kind``, ``fields``, ``start_date``
                and, optionally, ``end_date``.

                Example input:
                    >>> content_restrictions = {
                    ...     'datastore_kind': 'TransactionCompositeEntry',  # The only valid target kind.
                    ...     'fields': ['transaction_id', 'contract_date'],  # Inclusive list of accessible properties.
                    ...     'start_date': '2005-06-01',  # Lower query limit (based on the kind default sort property).
                    ...     'end_date': '2015-01-01'  # Upper subscription query limit, defaults to indefinite if unset.
                    ... }

        Returns:
            The RPC wrapper, configured with the handler's response.

        Raises:
            AssertionError on missing/empty parameters.
            ValueError on invalid keyword arguments.
        """

        for arg in [ancestor, quota, subscription_type, start_date, friendly_name, update_frequency,
                    content_restrictions]:
            assert arg is not None

        if subscription is not None:  # It's an _edit_ op.
            subscription_future = ndb.Key(urlsafe=subscription).get_async()
        else:  # It's an _add_ operation.
            subscription_future = None

        if subscription_type == 'data_api_subscription':
            subscription_type = constants.DATA_API_SUBSCRIPTION
        elif subscription_type == 'widget_service':
            subscription_type = constants.WIDGET_SERVICE_SUBSCRIPTION
        else:
            raise ValueError('Invalid \'subscription_type\' argument \'%s\'.' % subscription_type)

        description = description or None

        ancestor_key = self._resolve_subscriber_account(ancestor, key_only=True)

        quota = int(quota)
        if quota <= 0:
            raise ValueError('Invalid \'quota\' argument \'%s\'.' % quota)

        assert update_frequency in ['subscription_monthly_update_frequency', 'subscription_weekly_update_frequency',
                                    'subscription_daily_update_frequency']

        try:
            start_date = formatting.coerce_to_dt(start_date)
        except ValueError:
            raise ValueError('Invalid \'start_date\' argument \'%s\'.' % start_date)

        if end_date:
            try:
                end_date = formatting.coerce_to_dt(end_date)
            except ValueError:
                raise ValueError('Invalid \'end_date\' argument \'%s\'.' % end_date)

        restriction_kwargs = content_restrictions.copy()
        try:
            restriction_kwargs['start_date'] = formatting.coerce_to_dt(content_restrictions['start_date'])
        except ValueError:
            raise ValueError(
                'Invalid content restriction \'start_date\' argument \'%s\'.' % content_restrictions['start_date'])

        if 'end_date' in content_restrictions and content_restrictions['end_date']:
            try:
                restriction_kwargs['end_date'] = formatting.coerce_to_dt(content_restrictions['end_date'])
            except ValueError:
                raise ValueError(
                    'Invalid content restriction \'end_date\' argument \'%s\'.' % content_restrictions['end_date'])

        assert 'datastore_kind' in content_restrictions
        if content_restrictions['datastore_kind'] != 'TransactionCompositeEntry':
            raise ValueError('Invalid content restriction \'datastore_kind\' value \'%s\'.'
                             % content_restrictions['datastore_kind'])

        assert 'fields' in content_restrictions and isinstance(content_restrictions['fields'], (list, tuple))

        input_kwargs = {
            'subscription_type': subscription_type,
            'friendly_name': friendly_name,
            'description': description,
            'content_restrictions': user_model.SubscriptionContentRestriction(**restriction_kwargs),
            'quota': quota,
            'end_date': end_date,
            'start_date': start_date,
            'update_frequency': update_frequency
        }

        if subscription_future:  # We're still saying it's an _edit_ operation.
            subscription_entity = subscription_future.get_result()
            assert subscription_type == subscription_entity.subscription_type  # Plz no changing the subscription type.
            subscription_entity.populate(**input_kwargs)

        else:  # We're still staying it's an _add_ operation.
            subscription_entity = user_model.Subscription(
                parent=ancestor_key, api_access_key=user_model.Subscription.generate_api_access_key(), **input_kwargs)

        subscription_entity.put()

        return command.set_response_data({'success': True,
                                          'data': self._to_json(subscription_entity)})

    def suspend_subscription(self, command, subscription=None):
        u"""Updates a ``Subscription`` config entity within a ``SubscriberAccount`` to being `suspended`.

        Args:
            command: A ``Command`` RPC wrapper.
            subscription: A urlsafe ``Subscription`` key (required).

        Returns:
            The RPC wrapper, configured with the handler's response.
        """

        subscription_entity = self._resolve_subscription(subscription)
        command.add_futures(subscription_entity.set_suspended(True, delegate_future_mgnt=True))

        return command.set_response_data({'success': True,
                                          'data': subscription_entity.key.urlsafe()})

    def unsuspend_subscription(self, command, subscription=None):
        u"""Updates a ``Subscription`` config entity within a ``SubscriberAccount`` to _not_ being `suspended`.

        Args:
            command: A ``Command`` RPC wrapper.
            subscription: A urlsafe ``Subscription`` key (required).

        Returns:
            The RPC wrapper, configured with the handler's response.
        """

        subscription_entity = self._resolve_subscription(subscription)
        command.add_futures(subscription_entity.set_suspended(False, delegate_future_mgnt=True))

        return command.set_response_data({'success': True,
                                          'data': subscription_entity.key.urlsafe()})

    def delete_subscription(self, command, subscription=None):
        u"""Marks a ``Subscription`` config entity within a ``SubscriberAccount`` as having been `deleted`.

        Args:
            command: A ``Command`` RPC wrapper.
            subscription: A urlsafe ``Subscription`` key (required).

        Returns:
            The RPC wrapper, configured with the handler's response.
        """

        subscription_entity = self._resolve_subscription(subscription)
        command.add_futures(subscription_entity.set_deleted(True, delegate_future_mgnt=True))

        return command.set_response_data({'success': True,
                                          'data': subscription_entity.key.urlsafe()})

    def add_or_edit_user_account(self, command, user_account=None, ancestor=None, administrator=None,
                                 contact_details=None):
        u"""Adds or edits a ``UserAccount`` entity within a ``SubscriberAccount``.

        Args:
            command: A ``Command`` RPC wrapper.
            user_account: A urlsafe ``UserAccount`` key.
            ancestor: A urlsafe parent ``SubscriberAccount`` key (required).
            administrator: Usage quota limit (required).
            contact_details: A dict-like value containing name, email address, phone number, job title etc. Example:
                >>> contact_details = {
                ...     'first_name': 'Mats',  # Required first name.
                ...     'last_name': 'Blomdahl',  # Required last name.
                ...     'labels': ['su', 'admin_account'],  # Either ``admin_account`` or ``user_account`` required.
                ...     'email_addresses_support': 'mats.blomdahl@maklarstatistik.se',  # One or more email addresses.
                ...     'email_addresses_work': 'mats@stormfors.se',
                ...     'phone_numbers_work': '+46(0)730-567 567',  # Optional phone number.
                ...     'job_title': 'Automation Evangelist'  # Optional job title.
                ... }

        Returns:
            The RPC wrapper, configured with the handler's response.

        Raises:
            AssertionError on missing/empty parameters.
            ValueError on invalid keyword arguments.
        """

        assert ancestor is not None
        ancestor_key = self._resolve_subscriber_account(ancestor, key_only=True)

        assert contact_details is not None
        contact_details = self._parse_json_contact(contact_details)

        if user_account is not None:  # It's an _edit_ op.
            user_account_entity = ndb.Key(urlsafe=user_account).get()
            contact_details_entity = user_account_entity.contact_details.get()
            contact_details_entity.populate(**contact_details)

        else:  # It's an _add_ operation.
            user_account_entity = None
            contact_details_entity = ecore_generic_model.ContactDetails(parent=ancestor_key, **contact_details)

        assert administrator in [True, False]
        if administrator:
            assert 'admin_account' in contact_details_entity.labels
        else:
            assert 'user_account' in contact_details_entity.labels
        sys_user_role_category = constants.SUBSCRIBER_ADMIN if administrator else constants.SUBSCRIBER_USER

        contact_details_future = contact_details_entity.put_async()

        if user_account_entity:  # We're still saying it's an _edit_ operation.
            user_account_entity.populate(user_email=contact_details_entity.email_addresses[0].email_address,
                                         sys_user_role=sys_user_role_category,
                                         administrator=administrator,
                                         sys_origin_organization_account=command.user_account_entry.key.parent(),
                                         sys_origin_user_account=command.user_account_entry.key)
            user_account_future = user_account_entity.put_async()
            ndb.Future.wait_all([contact_details_future, user_account_future])

        else:  # We're still staying it's an _add_ operation.
            user_account_entity = user_model.UserAccount(
                parent=ancestor_key,
                contact_details=None,
                user_email=contact_details_entity.email_addresses[0].email_address,
                api_access_key=user_model.UserAccount.generate_api_access_key(),
                sys_user_role=sys_user_role_category,
                administrator=administrator,
                sys_pending_password_update_key=user_model.UserAccount.generate_password_update_key(),
                sys_pending_password_update_expiration_datetime=datetime.datetime.today()+datetime.timedelta(days=31),
                sys_origin_organization_account=command.user_account_entry.key.parent(),
                sys_origin_user_account=command.user_account_entry.key
            )

            # TODO(mats.blomdahl@gmail.com): Remove temporarily disabled assertion.
            # user_model.UserAccount.assert_account_is_unique(
            #     ancestor=ancestor_key,
            #     user_email=new_account_entry.user_email,
            #     username=new_account_entry.username
            # )

            new_account_key = user_account_entity.put()

            user_account_entity.populate(user_id=str(new_account_key.id()),
                                         contact_details=contact_details_future.get_result())
            user_account_entity.put()

        return command.set_response_data({'success': True,
                                          'data': self._to_json(user_account_entity)})

    def suspend_user_account(self, command, user_account=None):
        u"""Updates a ``UserAccount`` entity within a ``SubscriberAccount`` to being `suspended`.

        Args:
            command: A ``Command`` RPC wrapper.
            user_account: A urlsafe ``UserAccount`` key (required).

        Returns:
            The RPC wrapper, configured with the handler's response.
        """

        user_account_entity = self._resolve_user_account(user_account)
        command.add_futures(user_account_entity.set_suspended(True, delegate_future_mgnt=True))

        return command.set_response_data({'success': True,
                                          'data': user_account_entity.key.urlsafe()})

    def unsuspend_user_account(self, command, user_account=None):
        u"""Updates a ``UserAccount`` entity within a ``SubscriberAccount`` to _not_ being `suspended`.

        Args:
            command: A ``Command`` RPC wrapper.
            user_account: A urlsafe ``UserAccount`` key (required).

        Returns:
            The RPC wrapper, configured with the handler's response.
        """

        user_account_entity = self._resolve_user_account(user_account)
        command.add_futures(user_account_entity.set_suspended(False, delegate_future_mgnt=True))

        return command.set_response_data({'success': True,
                                          'data': user_account_entity.key.urlsafe()})

    def delete_user_account(self, command, user_account=None):
        u"""Marks a ``UserAccount`` entity within a ``SubscriberAccount`` as having been `deleted`.

        Args:
            command: A ``Command`` RPC wrapper.
            user_account: A urlsafe ``UserAccount`` key (required).

        Returns:
            The RPC wrapper, configured with the handler's response.
        """

        user_account_entity = self._resolve_user_account(user_account)
        command.add_futures(user_account_entity.set_deleted(True, delegate_future_mgnt=True))

        return command.set_response_data({'success': True,
                                          'data': user_account_entity.key.urlsafe()})

    def add_or_edit_subscriber_account(self, command, subscriber=None, company_identity_details=None,
                                       postal_addresses=None, contact_details=None, web_addresses=None):
        u"""Adds or edits a ``SubscriberAccount`` entity.

        When a new account is added, contact details for creating one or more initial ``UserAccount`` entities will also
            be required.

        Args:
            command: A ``Command`` RPC wrapper.
            subscriber: A urlsafe ``SubscriberAccount`` key.
            company_identity_details: A dict-like value with ``organization_name`` (required), ``department_name``
                and ``organization_id``.
            postal_addresses: A list of dict-like values representing postal addresses. Example:
                >>> postal_addresses = [{
                ...     'recipient_lines': [u'A3J Consulting AB', u'Avdelningen för IT-utveckling'],
                ...     'route_name': u'Kammakargatan',
                ...     'street_number': 48,
                ...     'postal_code': '11160',
                ...     'postal_town': u'Stockholm'
                ... }]
            contact_details: A list of dict-like values containing name, email address, job title etc. Example:
                >>> contact_details = [{
                ...     'first_name': 'Mats',  # Required first name.
                ...     'last_name': 'Blomdahl',  # Required last name.
                ...     'labels': ['su', 'admin_account'],  # Either ``admin_account`` or ``user_account`` required.
                ...     'email_addresses_work': 'mats.blomdahl@gmail.com',  # Required email address.
                ...     'phone_numbers_office': None,  # Optional phone number.
                ...     'phone_numbers_mobile': '+46(0)730-567 567',  # Optional phone number.
                ...     'job_title': None  # Optional job title.
                ... }, {
                ...     'first_name': 'Olof',
                ...     'last_name': 'Karlsson'
                ...     'labels': ['su', 'admin_account'],
                ...     'email_addresses_work': 'olof.karlsson@a3j.se',
                ...     'job_title': 'Developer'
                ... }]
            web_addresses: A list of dict-like values containing company web addresses. Example:
                >>> web_addresses = [{
                ...     'url': 'http://www.a3j.se',  # Required URL.
                ...     'labels': ['company_main']  # Optional labels.
                ... }]

        Returns:
            The RPC wrapper, configured with the handler's response.

        Raises:
            AssertionError on missing/empty parameters.
            ValueError on invalid keyword arguments.
        """

        if any(set(company_identity_details.keys()) - {'organization_name', 'department_name', 'organization_id'}):
            raise ValueError('Invalid company identity input \'%s\'.' % pprint.pformat(company_identity_details))

        if subscriber is not None:  # It's an _edit_ op.
            subscriber_entity = ndb.Key(urlsafe=subscriber).get()
            subscriber_entity.populate(**company_identity_details)
            subscriber_entity.put()

            return command.set_response_data({'success': True,
                                              'data': {'subscriber_account': self._to_json(subscriber_entity)}})

        else:  # It's an _add_ operation.
            default_password_update_expiration_dt = datetime.datetime.today() + datetime.timedelta(days=31)

            assert company_identity_details is not None
            subscriber_entity = user_model.SubscriberAccount(**company_identity_details)

            subscriber_entity.sys_origin_user_account = command.user_account_entry.key
            subscriber_entity.sys_origin_organization_account = command.user_account_entry.key.parent()

            logging.debug('[AdminCmd.add_or_edit_subscriber_account] command: \'%s\'' % pprint.pformat(command))
            logging.debug('[AdminCmd.add_or_edit_subscriber_account] subscriber_entity: \'%s\''
                          % pprint.pformat(subscriber_entity))

            user_model.SubscriberAccount.assert_account_is_unique(
                organization_name=subscriber_entity.organization_name,
                department_name=subscriber_entity.department_name,
                organization_id=subscriber_entity.organization_id
            )

            def _process_contact(raw_contact_input):
                contact_input = self._parse_json_contact(raw_contact_input)

                logging.debug(
                    '[AdminCmd.add_or_edit_subscriber_account._process_contact] Post-processed initial \'UserAccount\' '
                    'entity with primary email \'%s\'...' % contact_input['email_addresses'][0].email_address)

                # TODO(mats.blomdahl@gmail.com): Restored assertions.
                # try:
                #     user_model.UserAccount.assert_account_is_unique(user_email=user_email)
                # except AssertionError as e:
                #     return command.set_response_data({
                #         'status_code': ecore_constants.HTTP_BAD_REQUEST.get_status_code(),
                #         'errors': {
                #             'DUPLICATE USER IDENTIFIER': 'Cannot process duplicate user email \'%s\'. %s' % (
                #             contact_input['email_addresses'][0].email_address, e.message)
                #         }
                #     })

                return contact_input

            assert contact_details is not None
            parsed_contacts = [_process_contact(contact) for contact in contact_details]

            assert isinstance(postal_addresses, (list, tuple))

            new_subscriber_key = subscriber_entity.put()
            logging.info('[AdminCmd.add_or_edit_subscriber_account] New Subscriber entity \'%s\' written to Datastore.'
                         % new_subscriber_key)

            for address in postal_addresses:
                address = self._parse_json_postal_address(address)
                command.add_futures(ecore_generic_model.PostalAddress(parent=new_subscriber_key, **address).put_async())

            admin_account_contacts, user_account_contacts = [], []
            for contact in parsed_contacts:
                contact_entity = ecore_generic_model.ContactDetails(parent=new_subscriber_key, **contact)

                if 'admin_account' in contact_entity.labels:
                    admin_account_contacts.append((contact_entity.put_async(), contact_entity))

                elif 'user_account' in contact_entity.labels:
                    user_account_contacts.append((contact_entity.put_async(), contact_entity))

                else:
                    raise ValueError('Found neither \'admin_account\' nor \'user_account\' label for contact %s.'
                                     % contact_entity.email_addresses[0].email_address)

            if web_addresses:
                for address in web_addresses:
                    if any(set(address.keys()) - {'labels', 'url'}):
                        raise ValueError('Invalid web address input \'%s\'.' % pprint.pformat(address))
                    else:
                        command.add_futures(ecore_generic_model.WebAddress(parent=new_subscriber_key,
                                                                           **address).put_async())

            new_user_accounts, new_user_account_json = [], []
            for contact_future, contact_details in admin_account_contacts:
                contact_key = contact_future.get_result()
                logging.debug('[AdminCmd.add_or_edit_subscriber_account] New administrator ContactDetail entity \'%s\' '
                              'written to Datastore (contact_details: %s' % (contact_key,
                                                                             pprint.pformat(contact_details)))
                new_admin = user_model.UserAccount(
                    parent=ndb.Key(user_model.SubscriberAccount, new_subscriber_key.id()),
                    contact_details=contact_key,
                    user_email=contact_details.email_addresses[0].email_address,
                    api_access_key=user_model.UserAccount.generate_api_access_key(),
                    sys_user_role=constants.SUBSCRIBER_ADMIN,
                    administrator=True,
                    sys_pending_password_update_key=user_model.UserAccount.generate_password_update_key(),
                    sys_pending_password_update_expiration_datetime=default_password_update_expiration_dt,
                    sys_origin_organization_account=command.user_account_entry.key.parent(),
                    sys_origin_user_account=command.user_account_entry.key

                )

                new_user_accounts.append((new_admin.put_async(), new_admin))

            for contact_future, contact_details in user_account_contacts:
                contact_key = contact_future.get_result()
                logging.debug('[AdminCmd.add_or_edit_subscriber_account] New user ContactDetail entity \'%s\' written '
                              'to Datastore (contact_details: %s' % (contact_key, pprint.pformat(contact_details)))

                new_user = user_model.UserAccount(
                    parent=new_subscriber_key,
                    contact_details=contact_key,
                    user_email=contact_details.email_addresses[0].email_address,
                    sys_user_role=constants.SUBSCRIBER_USER,
                    administrator=False,
                    sys_pending_password_update_key=user_model.UserAccount.generate_password_update_key(),
                    sys_pending_password_update_expiration_datetime=default_password_update_expiration_dt,
                    sys_origin_organization_account=command.user_account_entry.key.parent(),
                    sys_origin_user_account=command.user_account_entry.key
                )

                new_user_accounts.append((new_user.put_async(), new_user))

            for account_future, account_entry in new_user_accounts:
                account_key = account_future.get_result()

                assert isinstance(account_key, ndb.Key)

                account_entry.user_id = str(account_key.id())

                command.add_futures(account_entry.put_async())

                new_user_account_json.append(self._to_json(account_entry))

            return command.set_response_data({'success': True,
                                              'data': {'subscriber_account': self._to_json(subscriber_entity),
                                                       'user_accounts': new_user_account_json}})

    def suspend_subscriber_account(self, command, subscriber_account=None):
        u"""Updates a ``SubscriberAccount`` entity, along with any descendant ``UserAccount`` or ``Subscription``
            entities, to being `suspended`.

        Args:
            command: A ``Command`` RPC wrapper.
            subscriber_account: A urlsafe ``SubscriberAccount`` key (required).

        Returns:
            The RPC wrapper, configured with the handler's response.
        """

        subscriber_account_key, subscriber_account_entry = self._resolve_subscriber_account(subscriber_account)

        command.add_futures(subscriber_account_entry.set_suspended(True, delegate_future_mgnt=True))

        user_accounts = user_model.UserAccount.query(ancestor=subscriber_account_key)
        subscriptions = user_model.Subscription.query(ancestor=subscriber_account_key)

        for user_account in user_accounts:
            try:
                command.add_futures(user_account.set_suspended(True, delegate_future_mgnt=True))
            except user_model_errors.UserUpdateRedundantError as e:
                logging.error('[AdminCmd.suspend_subscriber_account] Suspension of user account \'%s\' raised an user'
                              '_model_errors.UserUpdateRedundantError: \'%s\'' % (user_account.user_email, e.message))

        for subscription in subscriptions:
            try:
                command.add_futures(subscription.set_suspended(True, delegate_future_mgnt=True))
            except user_model_errors.SubscriptionUpdateRedundantError as e:
                logging.error('[AdminCmd.suspend_subscriber_account] Suspension of subscription \'%s\' raised an user'
                              '_model_errors.SubscriptionUpdateRedundantError: \'%s\'' % (subscription.friendly_name,
                                                                                          e.message))

        return command.set_response_data({'success': True,
                                          'data': subscriber_account_key.urlsafe()})

    def unsuspend_subscriber_account(self, command, subscriber_account=None):
        u"""Updates a ``SubscriberAccount`` entity, along with any descendant ``UserAccount`` or ``Subscription``
            entities, to _not_ being `suspended`.

        Args:
            command: A ``Command`` RPC wrapper.
            subscriber_account: A urlsafe ``SubscriberAccount`` key (required).

        Returns:
            The RPC wrapper, configured with the handler's response.
        """

        subscriber_account_key, subscriber_account_entry = self._resolve_subscriber_account(subscriber_account)

        command.add_futures(subscriber_account_entry.set_suspended(False, delegate_future_mgnt=True))

        user_accounts = user_model.UserAccount.query(ancestor=subscriber_account_key)
        subscriptions = user_model.Subscription.query(ancestor=subscriber_account_key)

        for user_account in user_accounts:
            try:
                command.add_futures(user_account.set_suspended(False, delegate_future_mgnt=True))
            except user_model_errors.UserUpdateRedundantError as e:
                logging.error('[AdminCmd.unsuspend_subscriber_account] Un-suspension of user account \'%s\' raised an '
                              'user_model_errors.UserUpdateRedundantError: \'%s\'' % (user_account.user_email,
                                                                                      e.message))

        for subscription in subscriptions:
            try:
                command.add_futures(subscription.set_suspended(False, delegate_future_mgnt=True))
            except user_model_errors.SubscriptionUpdateRedundantError as e:
                logging.error(
                    '[AdminCmd.unsuspend_subscriber_account] Un-suspension of subscription \'%s\' raised an user_model'
                    '_errors.SubscriptionUpdateRedundantError: \'%s\'' % (subscription.friendly_name, e.message))

        return command.set_response_data({'success': True,
                                          'data': subscriber_account_key.urlsafe()})

    def delete_subscriber_account(self, command, subscriber_account=None):
        u"""Marks a ``SubscriberAccount`` entity, along with any descendant ``UserAccount`` or ``Subscription``
            entities, as having been `deleted`.

        Args:
            command: A ``Command`` RPC wrapper.
            subscriber_account: A urlsafe ``SubscriberAccount`` key (required).

        Returns:
            The RPC wrapper, configured with the handler's response.
        """

        subscriber_account_key, subscriber_account_entry = self._resolve_subscriber_account(subscriber_account)

        command.add_futures(subscriber_account_entry.set_deleted(True, delegate_future_mgnt=True))

        user_accounts = user_model.UserAccount.query(ancestor=subscriber_account_key)
        subscriptions = user_model.Subscription.query(ancestor=subscriber_account_key)

        for user_account in user_accounts:
            try:
                command.add_futures(user_account.set_deleted(True, delegate_future_mgnt=True))
            except user_model_errors.UserUpdateRedundantError as e:
                logging.error('[AdminCmd.delete_subscriber_account] Deletion of user account \'%s\' raised an user'
                              '_model_errors.UserUpdateRedundantError: \'%s\'' % (user_account.user_email, e.message))

        for subscription in subscriptions:
            try:
                command.add_futures(subscription.set_deleted(True, delegate_future_mgnt=True))
            except user_model_errors.SubscriptionUpdateRedundantError as e:
                logging.error('[AdminCmd.delete_subscriber_account] Deletion of subscription \'%s\' raised an user'
                              '_model_errors.SubscriptionUpdateRedundantError: \'%s\'' % (subscription.friendly_name,
                                                                                          e.message))
        return command.set_response_data({'success': True,
                                          'data': subscriber_account_key.urlsafe()})

    # TODO(mats.blomdahl@gmail.com): Add docstring notice on ``flatten_datastore_refs`` and ``resolve_datastore_refs``.
    def chart_data(self, command, kind=None, offset=None, page_size=50, page=None, order_by='sys_modified',
                   order_direction='ASC', filter_by=None, group_by=None, resolve_datastore_refs=0,
                   flatten_datastore_refs=True):
        u"""Queries the Datastore to produce chart data for Ext JS `Chart` components.

        Args:
            command: A ``Command`` RPC wrapper.
            kind: Name of the target Datastore kind.
            page_size: Number of records per page to return.
            offset: Offset argument for the NDB query.
            page: Target page in the query result (note that ``offset`` and ``page`` are mutually redundant).
            order_by: Target sort property for the NDB query.
            order_direction: Sort direction for ``order_by``.
            filter_by: Filter properties for the NDB query.
            group_by: Result grouping (not implemented).
            subscriber_account: A urlsafe ``SubscriberAccount`` key (required).

        Returns:
            The RPC wrapper, configured with the handler's response.
        """

        if not command.session_valid_and_authenticated():
            return command.set_response_data({'status_code': ecore_constants.HTTP_FORBIDDEN.get_status_code(),
                                              'errors': {'SESSION INVALID': _SESSION_INVALID_ERROR_MSG}})
        try:
            kind_cls = self._resolve_datastore_kind(kind)
        except errors.AccessDeniedError as e:
            return command.set_response_data({'status_code': ecore_constants.HTTP_ACCESS_DENIED.get_status_code(),
                                              'errors': {'ACCESS DENIED': e.message}})
        except errors.BadRequestError as e:
            return command.set_response_data({'status_code': ecore_constants.HTTP_BAD_REQUEST.get_status_code(),
                                              'errors': {'BAD REQUEST': e.message}})

        return command.set_response_data(self._datastore_subset(kind_cls, page, page_size, offset, order_by,
                                                                order_direction, filter_by, group_by,
                                                                resolve_datastore_refs=resolve_datastore_refs,
                                                                flatten_datastore_refs=flatten_datastore_refs))

    # TODO(mats.blomdahl@gmail.com): Add docstring notice on ``flatten_datastore_refs`` and ``resolve_datastore_refs``.
    def grid_data(self, command, kind=None, offset=None, page_size=50, page=None, order_by='sys_modified',
                  order_direction='ASC', filter_by=None, group_by=None, resolve_datastore_refs=0,
                  flatten_datastore_refs=True, transaction_id=None):
        u"""Queries the Datastore to produce grid data for the Ext JS `GridView` component.

        Args:
            command: A ``Command`` RPC wrapper.
            kind: Name of the target Datastore kind.
            page_size: Number of records per page to return.
            offset: Offset argument for the NDB query.
            page: Target page in the query result (note that ``offset`` and ``page`` are mutually redundant).
            order_by: Target sort property for the NDB query.
            order_direction: Sort direction for ``order_by``.
            filter_by: Filter properties for the NDB query.
            group_by: Result grouping (not implemented).
            subscriber_account: A urlsafe ``SubscriberAccount`` key (required).

        Returns:
            The RPC wrapper, configured with the handler's response.
        """

        if not command.session_valid_and_authenticated():
            return command.set_response_data({'status_code': ecore_constants.HTTP_FORBIDDEN.get_status_code(),
                                              'errors': {'SESSION INVALID': _SESSION_INVALID_ERROR_MSG}})
        try:
            kind_cls = self._resolve_datastore_kind(kind)
        except errors.AccessDeniedError as e:
            return command.set_response_data({'status_code': ecore_constants.HTTP_FORBIDDEN.get_status_code(),
                                              'errors': {'ACCESS DENIED': e.message}})
        except errors.BadRequestError as e:
            return command.set_response_data({'status_code': ecore_constants.HTTP_BAD_REQUEST.get_status_code(),
                                              'errors': {'BAD REQUEST': e.message}})

        return command.set_response_data(self._datastore_subset(kind_cls, page, page_size, offset, order_by,
                                                                order_direction, filter_by, group_by,
                                                                resolve_datastore_refs=resolve_datastore_refs,
                                                                flatten_datastore_refs=flatten_datastore_refs,
                                                                transaction_id=transaction_id))
