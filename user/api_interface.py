# -*- coding: utf-8 -*-
#
# Title:   mstat_front-end_api.user.ApiInterface
# Author:  Mats Blomdahl
# Version: 2013-09-21

import logging
import re
import pprint

import mstat_generic_handler as generic_handler

import ext_direct_interface
from .. import decorators as decorators


class ApiInterface(generic_handler.CommonApiInterface):
    """Misc. handlers specific to the `User API` (deployed on the ``/user/api`` path).

    Security: All handlers are public and thus relies on explicit session management and user authentication.
    """

    _ROUTE = '/user/api'
    _EXT_DIRECT_HANDLERS = ext_direct_interface.UserCmd
    _INTERFACE_CLASS = 'user'
    _INTERFACE_FRIENDLY_NAME = 'user.ApiInterface'

    # Interface: /user/api/login
    def login(self, command):
        u"""Performs user login.

        Args:
            command: A ``Command`` RPC wrapper.

        Returns:
            A call to the ``subscriber_login`` handler.
        """

        ext_direct_handler = self._EXT_DIRECT_HANDLERS()
        return ext_direct_handler.subscriber_login(command, **command.payload)

    # Interface: /user/api/dquery
    def dquery(self, command):
        u"""The legacy query interface for the Mäklarstatistik `Data API Service` (now deprecated).

        Attempts best-effort normalization of query parameters. E.g. ``StartDate`` -> ``start_date``.

        Args:
            command: A ``Command`` RPC wrapper.

        Returns:
            A call to the ``query_subscription`` handler.
        """

        kwarg_keys_re = {
            'start_date': r'start_?date',
            'end_date': r'end_?date',
            'api_access_key': r'api_?access_?key',
            'api_version': r'api_?version',
            'output_format': r'output_?format',
            'naming_convention': r'naming_?convention',
            'dry_run': r'dry_?run',
            'preview': r'preview',
            'attachment': r'attachment',
            'filename': r'filename'
        }

        ext_direct_kwargs, lax_keys_matched = {}, set()
        logging.debug('[ApiInterface.dquery] command.payload: \'%s\'' % pprint.pformat(command.payload))
        logging.debug('[ApiInterface.dquery] dir(command.payload): \'%s\'' % pprint.pformat(dir(command.payload)))

        for strict_key, lax_key_re in kwarg_keys_re.iteritems():
            logging.debug('[ApiInterface.dquery] Evaluating strict_key \'%s\'...' % strict_key)
            for lax_key, input_value in command.payload.iteritems():
                if re.match(lax_key_re, lax_key, re.I):
                    logging.debug('[ApiInterface.dquery] Matched \'lax_key\' \'%s\' to \'%s\'.' % (lax_key, input_value))
                    ext_direct_kwargs[strict_key] = input_value
                    lax_keys_matched.add(lax_key)

        logging.debug('[ApiInterface.dquery] No match for \'lax_keys\' \'%s\'.'
                      % pprint.pformat(lax_keys_matched - set(command.payload.keys())))

        logging.info('[ApiInterface.dquery] Identified normalized kwargs: \n%s' % pprint.pformat(ext_direct_kwargs))

        ext_direct_handler = self._EXT_DIRECT_HANDLERS()
        return ext_direct_handler.alt_query_subscription(command, **ext_direct_kwargs)

    # Interface: /user/api/query
    def query(self, command):
        u"""Query interface for the Mäklarstatistik `Data API Service`.

        Attempts best-effort normalization of query parameters. E.g. ``StartDate`` -> ``start_date``.

        Args:
            command: A ``Command`` RPC wrapper.

        Returns:
            A call to the ``query_subscription`` handler.
        """

        kwarg_keys_re = {
            'start_date': r'start_?date',
            'end_date': r'end_?date',
            'api_access_key': r'api_?access_?key',
            'api_version': r'api_?version',
            'output_format': r'output_?format',
            'naming_convention': r'naming_?convention',
            'dry_run': r'dry_?run',
            'preview': r'preview',
            'attachment': r'attachment',
            'filename': r'filename'
        }

        ext_direct_kwargs, lax_keys_matched = {}, set()
        logging.debug('[ApiInterface.query] command.payload: \'%s\'' % pprint.pformat(command.payload))
        logging.debug('[ApiInterface.query] dir(command.payload): \'%s\'' % pprint.pformat(dir(command.payload)))

        for strict_key, lax_key_re in kwarg_keys_re.iteritems():
            logging.debug('[ApiInterface.query] Evaluating strict_key \'%s\'...' % strict_key)
            for lax_key, input_value in command.payload.iteritems():
                if re.match(lax_key_re, lax_key, re.I):
                    logging.debug('[ApiInterface.query] Matched \'lax_key\' \'%s\' to \'%s\'.' % (lax_key, input_value))
                    ext_direct_kwargs[strict_key] = input_value
                    lax_keys_matched.add(lax_key)

        logging.debug('[ApiInterface.query] No match for \'lax_keys\' \'%s\'.'
                      % pprint.pformat(lax_keys_matched - set(command.payload.keys())))

        logging.info('[ApiInterface.query] Identified normalized kwargs: \'%s\'' % pprint.pformat(ext_direct_kwargs))

        ext_direct_handler = self._EXT_DIRECT_HANDLERS()
        return ext_direct_handler.query_subscription(command, **ext_direct_kwargs)

    # Interface: /user/api/format_api_call
    def format_api_call(self, command):
        u"""Formats a JSON payload for submission to the Mäklarstatistik `Data API Service`.

        Args:
            command: A ``Command`` RPC wrapper.

        Returns:
            A call to the ``format_api_call`` handler.
        """

        ext_direct_handler = self._EXT_DIRECT_HANDLERS()
        return ext_direct_handler.format_api_call(command, **command.payload)

    # TODO(mats.blomdahl@gmail.com): Re-factor/deprecate.
    # Interface: /user/api/submit_api_call
    def submit_api_call(self, command):
        u"""Relays an API call to the Mäklarstatistik `Data API Service`.

        Args:
            command: A ``Command`` RPC wrapper.

        Returns:
            A call to the ``submit_api_call`` handler.
        """

        ext_direct_handler = self._EXT_DIRECT_HANDLERS()
        return ext_direct_handler.submit_api_call(command, **command.payload)

    # Interface: /user/api/daily_stats
    def daily_stats(self, command):
        u"""Loads system performance stats.

        Args:
            command: A ``Command`` RPC wrapper.

        Returns:
            A call to the ``daily_stats`` handler.
        """

        ext_direct_handler = self._EXT_DIRECT_HANDLERS()
        return ext_direct_handler.daily_stats(command, **command.payload)

