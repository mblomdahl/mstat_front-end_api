# -*- coding: utf-8 -*-
#
# Title:   mstat_front-end_api.user.UserCmd
# Author:  Mats Blomdahl, Olof Karlsson
# Version: 2014-04-27

import logging
import json
import datetime
import operator
import random
import time
import pprint
import urllib

from google.appengine.api import taskqueue
from google.appengine.api import urlfetch
from google.appengine.api import memcache
from google.appengine.ext import ndb

import ecore.utils as ecore_utils
import ecore.formatting as formatting

import ecore.constants as ecore_constants
import ecore.model.user as ecore_user_model
ecore_user_model_errors = ecore_user_model.errors

import mstat_model.user as user_model
user_model_errors = user_model.errors

import mstat_constants as constants

import mstat_model.transactions as transaction_model
import mstat_model.memcache as memcache_model

import mstat_generic_handler as generic_handler

import mstat_generic_handler.errors as handler_errors


_DEBUG = False

_SESSION_INVALID_ERROR_MSG = 'Cannot process request without a valid and authenticated session.'


class UserCmd(generic_handler.CommonExtDirectInterface):
    u"""Ext Direct endpoints/handlers specific to the `User API` (deployed on the ``/user/api`` path).

    Security: All handlers are public and thus relies on explicit session management and user authentication.
    """

    _ACCESSIBLE_TEMPLATES = {
        'safe_password_update_email': u"Lösenordsåterställning för Mäklarstatistiks informationstjänster"
    }
    _ACCESSIBLE_KINDS = {'Subscription': user_model.Subscription}
    _TO_JSON_DEFAULT_CFG = {'resolve_datastore_refs': 0,
                            'flatten_datastore_refs': True}

    def format_api_call(self, command, api_version=None, api_access_key=None, start_date=None, end_date=None,
                        naming_convention=None, output_format=None):
        u"""Formats a JSON payload for submission to the Mäklarstatistik `Data API Service`.

        Args:
            command: A ``Command`` RPC wrapper.
            api_version: Target API version (required).
            api_access_key: A fully qualified API access key for the `Data API Service` (required).
            start_date: Lower contract date limit (inclusive, required).
            end_date: Lower contract date limit (exclusive, required).
            naming_convention: Output naming convention.
            output_format: Output format.

        Returns:
            The RPC wrapper, configured with the handler's response.

        Raises:
            AssertionError on missing ``api_version``, ``api_access_key``, ``start_date`` or ``end_date`` arguments.
        """

        for arg in [api_version, api_access_key, start_date, end_date]:
            assert arg is not None

        json_params = {'apiVersion': api_version, 'apiAccessKey': api_access_key, 'startDate': start_date,
                       'endDate': end_date}

        if naming_convention:
            json_params['namingConvention'] = naming_convention

        if output_format:
            json_params['outputFormat'] = output_format

        return command.set_response_data({'success': True,
                                          'data': 'POST %s/user/api/query\n\n%s' % (constants.APP_DEFAULT_VERSION_URL,
                                                                                    json.dumps(json_params, indent=4))})

    def submit_api_call(self, command, api_version=None, api_access_key=None, start_date=None, end_date=None,
                        naming_convention=None, output_format=None, preview=True):
        u"""Submits an API call to the Mäklarstatistik `Data API Service` endpoint.

        Args:
            command: A ``Command`` RPC wrapper.
            api_version: Target API version (required).
            api_access_key: A fully qualified API access key for the `Data API Service` (required).
            start_date: Lower contract date limit (inclusive, required).
            end_date: Lower contract date limit (exclusive, required).
            naming_convention: Output naming convention.
            output_format: Output format.

        Returns:
            The RPC wrapper, configured with the handler's response.

        Raises:
            AssertionError on missing ``api_version``, ``api_access_key``, ``start_date`` or ``end_date`` arguments.
        """

        for arg in [api_version, api_access_key, start_date, end_date, preview]:
            assert arg is not None

        json_params = {'apiVersion': api_version, 'apiAccessKey': api_access_key, 'startDate': start_date,
                       'endDate': end_date, 'prettyPrint': 'true', 'preview': str(preview).lower()}

        if naming_convention:
            json_params['namingConvention'] = naming_convention

        if output_format:
            json_params['outputFormat'] = output_format

        # TODO(mats.blomdahl@gmail.com): Remove inconsistent/unnecessary async logic.
        rpc = urlfetch.create_rpc(deadline=60)
        urlfetch.make_fetch_call(rpc, '%s/user/api/query?%s' % (constants.APP_DEFAULT_VERSION_URL,
                                                                urllib.urlencode(json_params)))
        rpc.wait()
        result = rpc.get_result()

        if _DEBUG:
            logging.debug('[UserCmd.submit_api_call] result.status_code: %i / dir(result): \'%s\''
                          % (result.status_code, pprint.pformat(dir(result))))

        json_str = result.content
        if _DEBUG:
            logging.debug('[UserCmd.submit_api_call] json_str: \'%r\'' % json_str)

        if result.status_code == 200:
            return command.set_response_data({'success': True,
                                              'data': '200 OK\n\n%s' % json_str})
        else:
            return command.set_response_data({'success': True,
                                              'data': '%i ERROR\n\n%s' % (result.status_code, json_str)})

    def query_subscription(self, command, api_version=None, api_access_key=None, start_date=None, end_date=None,
                           naming_convention='standard', output_format=ecore_constants.JSON_OUTPUT, preview=False,
                           attachment=False, filename=None, dry_run=False):
        """Foo.Bar"""

        DEADLINE = 56
        LEASETIME = DEADLINE * 10
        WORKER_FEED_QUEUES = constants.DATA_API_WORKER_FEED_QUEUES
        WORKER_SEED_QUEUES = constants.DATA_API_WORKER_SEED_QUEUES

        PULL_TASK_DELETION_QUEUE = constants.WORKER_PULL_TASK_DELETION_QUEUE
        QUERY_PROCESSING_QUEUE = constants.DATA_API_QUERY_PROCESSING_QUEUE
        QUERY_MEMCACHER_QUEUE = constants.DATA_API_QUERY_MEMCACHER_QUEUE
        WORKER_MODULES = constants.DATA_API_WORKER_MODULES[:1]
        CACHER_MODULES = constants.DATA_API_WORKER_MODULES[1:]

        QUEUE_BUSY_PREFIX = 'DATA_API_QUERY_ACTIVE_'

        mem_client = memcache.Client()
        start_time = time.time()

        dt_today = datetime.datetime.today()
        dt_breakpoint = datetime.datetime(dt_today.year, dt_today.month, dt_today.day, 4, 0, 0)

        today = datetime.date.today()
        if dt_today <= dt_breakpoint:
            today -= datetime.timedelta(days=1)
        else:
            dt_breakpoint += datetime.timedelta(days=1)

        task_deletion_rpcs = list()
        task_deletion_queue = taskqueue.Queue(PULL_TASK_DELETION_QUEUE)

        memcacher_queue = taskqueue.Queue(QUERY_MEMCACHER_QUEUE)
        worker_queue = taskqueue.Queue(QUERY_PROCESSING_QUEUE)

        active_worker_feed_queue = None
        active_worker_seed_queue = None

        subscription_entry = None
        user_account_entry = None
        parent_organization_entry = None

        output_version = None
        output_fields = None

        aggregate_kind = memcache_model.TceAggregate
        aggregate_sort_property = aggregate_kind.contract_date

        attachment = attachment == 'true'
        preview = preview == 'true'

        def _to_csv(subentry, output_version, output_fields):
            """Foo."""
            #logging.info('subentry: %s' % subentry)
            csv_output = subentry.to_csv(output_version=output_version,
                                         include=output_fields,
                                         dialect=ecore_constants.BQ_CSV_WRITER_DIALECT)[1].encode(ecore_constants.DEFAULT_ENCODING)

            return csv_output

        def _to_json(subentry, output_version, output_fields):
            """Foo"""
            #logging.info('subentry: %s' % subentry)
            json_output = subentry.to_json(serialize=False, datastore_refs=False, resolve_datastore_refs=0,
                                           flatten_datastore_refs=False, include=output_fields, exclude=None)

            return json_output

        def _get_target_worker(seed=None):
            """bar"""

            if seed is None:
                return WORKER_MODULES[random.randrange(0, len(WORKER_MODULES))]
            else:
                return WORKER_MODULES[seed % len(WORKER_MODULES)]

        def _get_target_cacher(seed=None):
            if seed is None:
                return CACHER_MODULES[random.randrange(0, len(CACHER_MODULES))]
            else:
                return CACHER_MODULES[seed % len(CACHER_MODULES)]

        def delete_pull_task(queue_name, task_names):
            """bar"""

            if not isinstance(task_names, (list, tuple)):
                task_names = [task_names]

            task = taskqueue.Task(
                url='/backend/worker/pull_task_delete',
                params={
                    'queue_name': queue_name,
                    'task_names': json.dumps(task_names)
                }  # ,
                #target=_get_target_cacher(seed=len(task_deletion_rpcs))  # ,
                # countdown=6  # DEADLINE - (time.time() - start_time)
            )
            task_deletion_rpcs.append((task, task_deletion_queue.add_async(task)))

        def generate_worker_init_task(shutdown_key, index):
            """bar"""

            return taskqueue.Task(
                url='/backend/data_api_worker/query_processor',
                params={
                    'deadline': DEADLINE,
                    'shutdown_key': shutdown_key,
                    'start_time': float(start_time),
                    'input_queue': active_worker_feed_queue,
                },
                target=_get_target_worker(seed=index),
                countdown=0.1 + float(index) / 10
            )

        def generate_query_task(task_name, entry_keys, output_tag):
            """bar"""

            logging.debug('payload: %s' % {
                'output_fields': output_fields,
                'output_format': output_format,
                'output_version': output_version,
                'output_task_name': task_name,
                'entry_keys': [key.urlsafe() for key in entry_keys],
                'output_queue': active_worker_seed_queue,
                'output_tag': output_tag
            })
            logging.debug('')
            return taskqueue.Task(
                method='PULL',
                payload=json.dumps({
                    'output_fields': output_fields,
                    'output_format': output_format,
                    'output_version': output_version,
                    'output_task_name': task_name,
                    'entry_keys': [key.urlsafe() for key in entry_keys],
                    'output_queue': active_worker_seed_queue,
                    'output_tag': output_tag
                })
            )

        def generate_caching_pull_task(entry_keys):
            """Foo"""

            return taskqueue.Task(
                method='PULL',
                payload=json.dumps({
                    'output_fields': output_fields,
                    'output_format': output_format,
                    'output_version': output_version,
                    # 'output_task_name': task_name,
                    'entry_keys': [key.urlsafe() for key in entry_keys],
                    # 'output_queue': active_worker_seed_queue,
                    # 'output_tag': output_tag
                }),
                countdown=2
            )

        def generate_caching_push_task(entry_key, index):
            """foo"""

            if False:  # index <= (len(WORKER_MODULES) * 2):
                target = WORKER_MODULES[index % len(WORKER_MODULES)]
                countdown = 6
                deadline = 12
            else:
                target = CACHER_MODULES[index % len(CACHER_MODULES)]
                countdown = 0
                deadline = 30

            return taskqueue.Task(
                url='/backend/data_api_worker/query_memcacher',
                payload=json.dumps({
                    'output_fields': output_fields,
                    'output_format': output_format,
                    'output_version': output_version,
                    'entry_key': entry_key.urlsafe(),
                    'start_time': float(start_time),
                    'deadline': deadline
                }),
                target=_get_target_cacher(seed=index)  # ,
                # countdown=countdown
            )

        def generate_default_filename():
            """Foo."""

            return '%s_%s-%s_api_output' % (dt_today.strftime('%Y%m%dT%H%M%S'), effective_start_date.strftime('%y%m%d'), effective_end_date.strftime('%y%m%d'))

        def generate_csv_output(response, filename=None, attachment=False):
            """Foo."""

            response_wrapper = {
                'success': True,
                'headers': {
                    'Content-Type': 'text/csv'
                },
                'data': None
            }

            if attachment:
                # Serve response as attachment
                if filename is None:
                    filename = generate_default_filename()
                response_wrapper['headers']['Content-Disposition'] = 'attachment; filename=%s.csv' % filename

            response_wrapper['data'] = response

            return response_wrapper

        def generate_json_output(response, preview=False, filename=None, attachment=False):
            """Foo."""

            response_wrapper = {
                'success': True,
                'headers': {
                    'Content-Type': 'application/json'
                },
                'meta': None,
                'data': None
            }

            if attachment:
                # Serve response as attachment
                if filename is None:
                    filename = generate_default_filename()
                response_wrapper['headers']['Content-Disposition'] = 'attachment; filename=%s.json' % filename
                response_wrapper.update(response)

            elif preview:
                # Pretty-print plain text response
                response_wrapper['headers']['Content-Type'] = 'text/plain'
                response_wrapper['data'] = json.dumps(response, sort_keys=True, indent=4)

            else:
                # Standard JSON output
                response_wrapper.update(response)

            return response_wrapper

        def assert_presence_and_basestring(str_obj, length=None):
            """bar"""

            if not isinstance(str_obj, (str, unicode)):
                return False

            if length is not None and len(str_obj.strip()) != length:
                return False

            return True

        def parse_iso_dateformat(date_str):
            """bar"""

            return datetime.date.fromordinal(datetime.datetime.strptime(date_str, '%Y-%m-%d').toordinal())

        def translate_usec(src_field_name, target_field_name=None):
            """bar"""

            if not target_field_name:
                target_field_name = src_field_name

            return "STRFTIME_UTC_USEC(TIMESTAMP_TO_USEC(%s), '%%Y-%%m-%%d %%H:%%M') as %s" % (src_field_name, target_field_name)

        def generate_sql(field_inclusion, dt_fields, field_name_translations, start_date, end_date, src_tables):

            def translate_usec(src_field_name, target_field_name=None):
                if not target_field_name:
                    target_field_name = src_field_name
                # TODO: check S or s below
                return "STRFTIME_UTC_USEC(TIMESTAMP_TO_USEC(%s), '%%Y-%%m-%%dT%%H:%%M:%%s') as %s" % (src_field_name, target_field_name)

            output_fieldnames = []

            query_fields = []

            for field in field_inclusion:
                output_fieldname = None
                if field in field_name_translations:
                    output_fieldname = field_name_translations[field]

                else:
                    output_fieldname = field

                output_fieldnames.append(output_fieldname)

                if field in dt_fields:
                    query_fields.append(translate_usec(field, output_fieldname))

                elif output_fieldname != field:
                    query_fields.append('%s as %s' % ( field, output_fieldname))

                else:
                    query_fields.append(output_fieldname)

            select_what = ', '.join(query_fields)
            select_from = ', '.join(src_tables)
            select_where = 'contract_date >= SEC_TO_TIMESTAMP(%i)' % formatting.timestamp_to_posix(start_date)
            select_where = '%s AND contract_date < SEC_TO_TIMESTAMP(%i)' % (select_where, formatting.timestamp_to_posix(end_date))
            order_by = 'contract_date'
            if order_by in field_name_translations:
                order_by = field_name_translations[order_by]

            return output_fieldnames, 'SELECT %s FROM %s WHERE %s ORDER BY %s' % (
                select_what, select_from, select_where, order_by)

        # Validate target date interval (´start_date´ and ´end_date´)
        if assert_presence_and_basestring(start_date):
            try:
                start_date = parse_iso_dateformat(start_date)
            except ValueError:
                return command.set_response_data({
                    'status_code': ecore_constants.HTTP_BAD_REQUEST.get_status_code(),
                    'errors': {
                        'START DATE MALFORMED': 'Invalid/malformed \'startDate\' argument (%r). The system will only accept ISO date format, e.g. \'2011-10-17\'.' % start_date
                    }
                })
        else:
            return command.set_response_data({
                'status_code': ecore_constants.HTTP_BAD_REQUEST.get_status_code(),
                'errors': {
                    'START DATE MISSING': 'Required \'startDate\' argument could not be resolved.'
                }
            })

        if assert_presence_and_basestring(end_date):
            try:
                end_date = parse_iso_dateformat(end_date)
                if (end_date - start_date).days < 1:
                    return command.set_response_data({
                        'status_code': ecore_constants.HTTP_BAD_REQUEST.get_status_code(),
                        'errors': {
                            'ILLEGAL DATE INTERVAL': 'Illegal date interval (%s--%s). Queries must span at least 1 day (you queried for %i days). For maximum performance and reliability, querying for 14-day intervals is highly recommended.' % (start_date.isoformat(), end_date.isoformat(), (end_date - start_date).days)
                        }
                    })

                if end_date > today:
                    return command.set_response_data({
                        'status_code': ecore_constants.HTTP_BAD_REQUEST.get_status_code(),
                        'errors': {
                            'ILLEGAL END DATE': 'Illegal \'end_date\' argument (%r). Transactions future from today (i.e. %s) cannot be queried (what date is considered \'today\' updates 04:00 UTC every morning, next update in %s).' % (end_date.isoformat(), today.isoformat(), formatting.timedelta_strftime(dt_breakpoint - dt_today, output_format='%(hours)ih %(minutes)im'))
                        }
                    })

            except ValueError as e:
                return command.set_response_data({
                    'status_code': ecore_constants.HTTP_BAD_REQUEST.get_status_code(),
                    'errors': {
                        'END DATE MALFORMED': 'Invalid/malformed \'end_date\' argument (%r). The system will only accept ISO date format, e.g. \'2012-01-17\'.' % end_date
                    }
                })

        else:
            return command.set_response_data({
                'status_code': ecore_constants.HTTP_BAD_REQUEST.get_status_code(),
                'errors': {
                    'END DATE MISSING': 'Required \'end_date\' argument could not be resolved.'
                }
            })

        # Workers Start-Up
        interval_len = (end_date - start_date).days

        busy_feed_queues = mem_client.get_multi(WORKER_FEED_QUEUES, key_prefix=QUEUE_BUSY_PREFIX)
        set_busy_future = None
        for queue in WORKER_FEED_QUEUES:
            if queue not in busy_feed_queues:
                active_worker_feed_queue = queue
                set_busy_future = mem_client.set_multi_async({active_worker_feed_queue: 'BUSY'},
                                                             key_prefix=QUEUE_BUSY_PREFIX, time=DEADLINE)
                break

        if active_worker_feed_queue is None:
            active_worker_feed_queue = WORKER_FEED_QUEUES[random.randrange(0, len(WORKER_FEED_QUEUES))]
            set_busy_future = mem_client.set_multi_async({active_worker_feed_queue: 'BUSY'},
                                                         key_prefix=QUEUE_BUSY_PREFIX, time=DEADLINE)

        assert set_busy_future.get_result() is not None  # TODO(mats.blomdahl@gmail.com): Add error handling behaviour.

        base_name = '%s_%i' % (api_access_key, (time.time() * 1e6))

        shutdown_key_name = '%s_QUERY_PROCESSOR_SHUTDOWN_KEY' % base_name
        shutdown_keys = ['%s_%i' % (shutdown_key_name, i) for i in range(interval_len if interval_len > len(WORKER_MODULES) else len(WORKER_MODULES))]

        for i in range(len(shutdown_keys)):
            worker_queue.add_async(generate_worker_init_task(shutdown_keys[i], i))

        busy_seed_queues_future = mem_client.get_multi_async(WORKER_SEED_QUEUES, key_prefix=QUEUE_BUSY_PREFIX)

        logging.info('%i workers triggered' % len(shutdown_keys))

        # Validate ´api_version´
        if api_version is None:
            return command.set_response_data({
                'status_code': ecore_constants.HTTP_BAD_REQUEST.get_status_code(),
                'errors': {
                    'API VERSION MISSING': 'Required \'apiVersion\' argument could not be resolved.'
                }
            })
        else:
            api_version = int(api_version)

            if api_version != 1:
                return command.set_response_data({
                    'status_code': ecore_constants.HTTP_BAD_REQUEST.get_status_code(),
                    'errors': {
                        'INVALID API VERSION': 'Malformed/invalid \'apiVersion\' argument (%r). The only available version (and valid input) is currently \'1\'.' % api_version
                    }
                })

        # Validate ´api_access_key´
        if assert_presence_and_basestring(api_access_key):
            try:
                subscription_entry, user_account_entry, \
                    futures = self._resolve_api_access_key(api_access_key.strip(), delegate_future_mgnt=True)
                command.add_futures(futures)
                output_fields = subscription_entry.content_restrictions.to_dict()['fields']
                parent_organization_entry_future = subscription_entry.key.parent().get_async(use_memcache=True)

            except (ecore_user_model_errors.ApiAccessKeyInvalidError, user_model_errors.SubscriptionNotFoundError,
                    ecore_user_model_errors.UserNotFoundError) as e:
                return command.set_response_data({
                    'status_code': ecore_constants.HTTP_BAD_REQUEST.get_status_code(),
                    'errors': {
                        'INVALID API ACCESS KEY': 'Malformed/invalid \'apiAccessKey\' argument (%r).' % api_access_key
                    }
                })

            except ecore_user_model_errors.UserSuspendedError as e:
                return command.set_response_data({
                    'status_code': ecore_constants.HTTP_FORBIDDEN.get_status_code(),
                    'errors': {
                        'USER ACCOUNT SUSPENDED': 'Cannot process query due to suspension. %s' % e.message
                    }
                })

            except ecore_user_model_errors.UserDeletedError as e:
                return command.set_response_data({
                    'status_code': ecore_constants.HTTP_FORBIDDEN.get_status_code(),
                    'errors': {
                        'USER ACCOUNT DELETED': 'Cannot process query due to deletion. %s' % e.message
                    }
                })

            except ecore_user_model_errors.ParentAccountDeletedError as e:
                return command.set_response_data({
                    'status_code': ecore_constants.HTTP_FORBIDDEN.get_status_code(),
                    'errors': {
                        'PARENT ACCOUNT DELETED': 'Cannot process query due to deletion. %s' % e.message
                    }
                })

            except ecore_user_model_errors.ParentAccountSuspendedError as e:
                return command.set_response_data({
                    'status_code': ecore_constants.HTTP_FORBIDDEN.get_status_code(),
                    'errors': {
                        'PARENT ACCOUNT SUSPENDED': 'Cannot process query due to suspension. %s' % e.message
                    }
                })

            except user_model_errors.SubscriptionSuspendedError as e:
                return command.set_response_data({
                    'status_code': ecore_constants.HTTP_FORBIDDEN.get_status_code(),
                    'errors': {
                        'SUBSCRIPTION SUSPENDED': 'Cannot process query due to suspension. %s' % e.message
                    }
                })

            except user_model_errors.SubscriptionDeletedError as e:
                return command.set_response_data({
                    'status_code': ecore_constants.HTTP_FORBIDDEN.get_status_code(),
                    'errors': {
                        'SUBSCRIPTION DELETED': 'Cannot process query due to deletion. %s' % e.message
                    }
                })

        else:
            return command.set_response_data({
                'status_code': ecore_constants.HTTP_BAD_REQUEST.get_status_code(),
                'errors': {
                    'API ACCESS KEY MISSING': 'Required \'apiAccessKey\' argument could not be resolved.'
                }
            })

        # Validate ´output_format´
        if assert_presence_and_basestring(output_format):
            output_format = output_format.lower().strip()
            if output_format not in [ecore_constants.JSON_OUTPUT, ecore_constants.CSV_OUTPUT]:
                return command.set_response_data({
                    'status_code': ecore_constants.HTTP_BAD_REQUEST.get_status_code(),
                    'errors': {
                        'INVALID OUTPUT FORMAT': 'Malformed/invalid \'outputFormat\' argument. Valid options include \'json\' and \'csv\'.'
                    }
                })

        else:
            raise AssertionError('No output_format?!')

        # Configure ´output_version´
        output_version = ecore_constants.DEFAULT_CSV
        if output_format == ecore_constants.JSON_OUTPUT:
            output_version = ecore_constants.BQ_JSON

        # Validate ´naming_convention´
        if assert_presence_and_basestring(naming_convention):
            naming_convention = naming_convention.strip().lower()

            if naming_convention not in ['legacy', 'standard', 'default']:
                return command.set_response_data({
                    'status_code': ecore_constants.HTTP_BAD_REQUEST.get_status_code(),
                    'errors': {
                        'INVALID NAMING CONVENTION': 'Malformed/invalid \'namingConvention\' argument. Valid options include \'default\', \'standard\' and \'legacy\'.'
                    }
                })

            if naming_convention == 'legacy':
                if output_format == ecore_constants.JSON_OUTPUT:
                    output_version = ecore_constants.LEGACY_JSON
                else:
                    output_version = ecore_constants.LEGACY_CSV

            kind_schema, kind_fields = transaction_model.TransactionCompositeEntry.get_kind_schema(
                output_format=ecore_constants.CSV_OUTPUT,
                output_version=ecore_constants.LEGACY_CSV,  # ugly hack.
                include_kind_fieldnames=True
            )

            schema_fields = kind_schema.get_fields()
            #for i in range(len(kind_fields)):
            #    if kind_fields[i] != schema_fields[i].get_name():
            #        field_name_translations[kind_fields[i]] = schema_fields[i].get_name()
            #        # logging.info('translating %s to %s' % (kind_fields[i], schema_fields[i].get_name()))

            logging.debug('kind_schema: %r' % kind_schema)
            logging.debug('kind_fields: %r' % kind_fields)
            logging.debug('schema_fields: %r' % schema_fields)

        # Validate target date interval based on subscription content restrictions
        content_restrictions = subscription_entry.content_restrictions
        if start_date < content_restrictions.start_date:
            return command.set_response_data({
                'status_code': ecore_constants.HTTP_FORBIDDEN.get_status_code(),
                'errors': {
                    'ILLEGAL START DATE': 'Illegal \'startDate\' argument (%r). The subscription\'s lower query limit is set to %s.' % (start_date.isoformat(), content_restrictions.start_date.isoformat())
                }
            })

        if content_restrictions.end_date and end_date > content_restrictions.end_date:
            return command.set_response_data({
                'status_code': ecore_constants.HTTP_FORBIDDEN.get_status_code(),
                'errors': {
                    'ILLEGAL END DATE': 'Illegal \'endDate\' argument (%r). The subscription\'s upper query limit is set to %s.' % (end_date.isoformat(), content_restrictions.end_date.isoformat())

                }
            })

        if (end_date - start_date).days > 32:
            return command.set_response_data({
                'status_code': ecore_constants.HTTP_INTERNAL_SERVER_ERROR.get_status_code(),
                'errors': {
                    'SERVER ERROR': 'Date range limit exceeded. The number of days queried by a single API call must not exceed 32 (your query covered %i days). For maximum performance and reliability, querying for 14-day intervals is highly recommended.' % (end_date - start_date).days
                }
            })

        logging.info('params:\n    output_version=%r, api_access_key=%r, start_date=%s, end_date=%s' % (
            output_version, api_access_key, start_date, end_date))

        busy_seed_queues = busy_seed_queues_future.get_result()

        for queue in WORKER_SEED_QUEUES:
            if queue not in busy_seed_queues:
                active_worker_seed_queue = queue
                set_busy_future = mem_client.set_multi_async({active_worker_seed_queue: 'BUSY'}, key_prefix=QUEUE_BUSY_PREFIX, time=DEADLINE)
                break

        if active_worker_seed_queue is None:
            active_worker_seed_queue = WORKER_SEED_QUEUES[random.randrange(0, len(WORKER_SEED_QUEUES))]
            set_busy_future = mem_client.set_multi_async({active_worker_seed_queue: 'BUSY'}, key_prefix=QUEUE_BUSY_PREFIX, time=DEADLINE)

        assert set_busy_future is not None

        # Restrict to subscription ´update_frequency´ config
        effective_start_date, effective_end_date = subscription_entry.restrict_effective_query_date_range(start_date, end_date)
        logging.debug('effective date interval: %s--%s' % (effective_start_date, effective_end_date))
        query = aggregate_kind.query(ndb.AND(aggregate_sort_property >= effective_start_date, aggregate_sort_property < effective_end_date))
        query_job_rpcs = []
        query_jobs = {}
        query_results = []
        task_keys = {}

        query_results_tag = '%s_%s_%s' % (base_name, start_date.strftime('%Y%m%d'), end_date.strftime('%Y%m%d'))
        query_jobs[query_results_tag] = []
        feed_queue = taskqueue.Queue(active_worker_feed_queue)

        def _to_feed_queue(entry):
            """Bar."""

            #task_names = ['%s_SHARD_%s' % (query_results_tag, key.id()[:10]) for key in entry.shard_keys]
            task_name = '%s_%s' % (base_name, entry.contract_date.strftime('%Y%m%d'))

            task = generate_query_task(task_name, entry.shard_keys, query_results_tag)

            query_job_rpcs.append((task, feed_queue.add_async(task)))

            query_jobs[query_results_tag].append(task_name)

            task_keys[task_name] = entry.shard_keys

            return entry

        logging.info('pre-map')
        aggregate_entries = query.map(_to_feed_queue, limit=interval_len, batch_size=interval_len, use_memcache=True)
        logging.info('post-map, query_rpcs %i' % len(query_job_rpcs))

        ex_future = transaction_model.TransactionCompositeEntry.query().get_async(deadline=3600, use_memcache=True)

        if output_format == ecore_constants.CSV_OUTPUT:
            output_fn = _to_csv
        else:
            output_fn = _to_json

        cache_start_date = effective_end_date
        cache_end_date = min(cache_start_date + datetime.timedelta(days=(interval_len + 1)), today)

        # Restrict to subscription ´update_frequency´ config
        cache_start_date, cache_end_date = subscription_entry.restrict_effective_query_date_range(cache_start_date, cache_end_date)
        cache_query = aggregate_kind.query(ndb.AND(aggregate_sort_property >= cache_start_date, aggregate_sort_property < cache_end_date))
        cache_interval_len = (cache_end_date - cache_start_date).days

        caching_job_rpcs = []

        def _to_cache_queue(entry):
            """Bar."""

            #logging.info('caching_queued for date %s' % entry.contract_date.isoformat())
            if True:  # len(entry.shard_keys) > 1:
                for entry_key in entry.shard_keys:
                    #if False len(caching_job_rpcs) <= interval_len:
                    #    caching_job_rpcs.append(feed_queue.add_async(generate_caching_pull_task([entry.shard_keys[i]])))
                    #else:
                    caching_job_rpcs.append(memcacher_queue.add_async(generate_caching_push_task(entry_key, len(caching_job_rpcs))))

            return None

        if cache_interval_len > 0:
            cache_future = cache_query.map_async(_to_cache_queue, limit=cache_interval_len, batch_size=cache_interval_len, use_memcache=True)
        else:
            cache_future = None

        while any(query_job_rpcs):
            retries = []
            for task, rpc in query_job_rpcs:
                try:
                    rpc.check_success()
                except taskqueue.TransientError as e:
                    logging.error('query_job_rpcs: %s' % e.message)
                    retries.append((task, feed_queue.add_async(task)))
                except BaseException as e:
                    retries.append((task, feed_queue.add_async(task)))
                    logging.error('unknown error: %s' % e.message)
            query_job_rpcs = retries

        # Fetch parent organization entry
        parent_organization_entry = parent_organization_entry_future.get_result()

        ex_entry = ex_future.get_result()

        if output_format == ecore_constants.CSV_OUTPUT:
            ex_entry_csv_label = ex_entry.to_csv(output_version=output_version, include=output_fields,
                                                 dialect=ecore_constants.BQ_CSV_WRITER_DIALECT)
            logging.debug('ex_entry_label: %rX\nX%r' % ex_entry_csv_label)
            logging.debug('ex_entry_label: %sX\nX%s' % ex_entry_csv_label)
            query_results = [[ex_entry_csv_label[0].encode(ecore_constants.DEFAULT_ENCODING)]]

        if cache_future:
            cache_future.wait()

        mem_keys = {}
        memcache_rpcs = []
        last_logmsg = None
        seed_queue = taskqueue.Queue(active_worker_seed_queue)
        retries_leq_3_count = 0
        db_futures = {}
        max_leq_3_retries = 12
        while any(aggregate_entries) and any(query_jobs):
            delete_tags = []
            worker_shutdown_keys = []
            tasks_leased = 0

            result_tags = query_jobs.keys()

            success_rpcs = []
            delete_rpcs = []

            # prefetch from ndb (in case of taskqueue failure)
            if retries_leq_3_count == 1 and not any(db_futures):
                for task_name in query_jobs[query_results_tag]:
                    db_futures[task_name] = ndb.get_multi_async(task_keys[task_name])

            while any(result_tags):
                lease_rpcs = []

                for query_results_tag in result_tags:
                    lease_rpcs.append((query_results_tag, seed_queue.lease_tasks_by_tag_async(LEASETIME, len(query_jobs[query_results_tag]), tag=query_results_tag)))

                for query_results_tag, rpc in lease_rpcs:
                    try:
                        rpc.check_success()
                        success_rpcs.append((query_results_tag, rpc))
                        result_tags.remove(query_results_tag)
                    except taskqueue.TransientError as e:
                        logging.error('lease_rpcs: %s' % e.message)

                    except BaseException as e:
                        logging.error('unknown error: %s' % e.message)

                if any(result_tags):
                    time.sleep(0.25)

            if any(memcache_rpcs):
                for keys, rpc in memcache_rpcs:
                    result = rpc.get_result()
                    if result is None:
                        logging.debug('rpc.get_result(): None')
                        time.sleep(0.25)
                        continue
                    for mem_key, data in result.iteritems():
                        del mem_keys[mem_key]
                        query_results.append(data)
                memcache_rpcs = []

            for query_results_tag, rpc in success_rpcs:
                #logging.debug('names: %s' % names)
                #try:
                tasks = rpc.get_result()
                #except taskqueue.TransientError as e:

                for task in tasks:
                    if len(query_jobs) < 3:
                        logging.info('tags_remaining / query_results_tag / task.name (task.size): %i / %s / %s (%i)' % (
                            len(query_jobs[query_results_tag]), query_results_tag, task.name, task.size))
                    query_jobs[query_results_tag].remove(task.name)
                    #logging.info('task.payload: %r' % task.payload)
                    task_data = json.loads(task.payload)
                    mem_keys.update(task_data['mem_keys'])
                    try:
                        shutdown_keys.remove(task_data['shutdown_key'])
                        worker_shutdown_keys.append(task_data['shutdown_key'])
                    except ValueError as e:
                        logging.error('shutdown_key / error: %s / %s' % (task_data['shutdown_key'], e.message))

                if any(tasks):
                    if not any(query_jobs[query_results_tag]):
                        delete_tags.append(query_results_tag)
                    tasks_leased += len(tasks)
                    #logging.error('missing tasks for output tag %s: %s' % (query_results_tag, query_jobs[query_results_tag]))
                    delete_pull_task(active_worker_seed_queue, [task.name for task in tasks])
                    #delete_rpcs.append((tasks, query_results_queue.delete_tasks_async(tasks)))

            for tag in delete_tags:
                del query_jobs[tag]

            if tasks_leased == 0:
                query_jobs_str = str(query_jobs)
                if len(query_jobs_str) > 500:
                    query_jobs_str = query_jobs_str[:500]

                logmsg = 'tasks_leased: %i (query_jobs: %s)' % (tasks_leased, query_jobs_str)
                if last_logmsg != logmsg:
                    logging.debug(logmsg)
                    last_logmsg = logmsg

                if (DEADLINE - (time.time() - start_time)) < 25:
                    if len(query_jobs[query_results_tag]) <= 3:
                        retries_leq_3_count += 1

                    if retries_leq_3_count == max_leq_3_retries:
                        for task_name in query_jobs[query_results_tag]:
                            for future in db_futures[task_name]:
                                entry = future.get_result()
                                logging.debug('manually, for taskqueue / entry.key.id(): %s' % entry.key.id())
                                tce_entries = entry.get_entries()
                                query_results.append([output_fn(e, output_version, output_fields) for e in tce_entries])
                        break

                time.sleep(0.5)

            else:
                retries_leq_3_count = 0
                if any(mem_keys):
                    memcache_rpcs.append((mem_keys, mem_client.get_multi_async(mem_keys.keys())))
                #if delete_tags:
                #shutdown_keys, tag_keys = shutdown_keys[:-len(delete_tags)], shutdown_keys[-len(delete_tags):]
                #shutdown_keys, tag_keys = shutdown_keys[:-tasks_leased], shutdown_keys[-tasks_leased:]
                mem_client.set_multi_async(dict.fromkeys(worker_shutdown_keys, True), time=120)

        if shutdown_keys:
            mem_client.set_multi_async(dict.fromkeys(shutdown_keys, True), time=120)

        mem_client.delete_multi_async([active_worker_seed_queue, active_worker_feed_queue],
                                      key_prefix=QUEUE_BUSY_PREFIX)

        logging.info('fetched %i query results, %i keys remaining (%s)' % (len(query_results), len(mem_keys), mem_keys))

        retries_count = 0
        while any(memcache_rpcs):
            for keys, rpc in memcache_rpcs:
                result = rpc.get_result()
                if result is None:
                    logging.debug('rpc.get_result() / keys: None / %s' % keys)
                    time.sleep(0.25)
                    continue
                for mem_key, data in result.iteritems():
                    del mem_keys[mem_key]
                    query_results.append(data)

            memcache_rpcs = []
            if any(mem_keys):
                if retries_count < 3:
                    memcache_rpcs.append((mem_keys, mem_client.get_multi_async(mem_keys.keys())))
                    retries_count += 1
                    time.sleep(0.25)
                else:
                    break

            logging.info('fetched %i query results, %i keys remaining (%s)' % (
                len(query_results), len(mem_keys), mem_keys))

        if any(mem_keys):
            # Resolve missing pieces manually
            db_futures = ndb.get_multi_async([ndb.Key(urlsafe=ndb_key_urlsafe) for ndb_key_urlsafe in mem_keys.values()])

            for future in db_futures:
                entry = future.get_result()
                logging.debug('manually, for memcache / entry.key.id(): %s' % entry.key.id())
                tce_entries = entry.get_entries()
                query_results.append([output_fn(e, output_version, output_fields) for e in tce_entries])

        while any(task_deletion_rpcs):
            # Wait for pull task deletion RPCs to succeed
            retries = []
            for task, rpc in task_deletion_rpcs:
                try:
                    rpc.check_success()
                except taskqueue.TransientError as e:
                    logging.error('task_deletion_rpcs: %s' % e.message)
                    retries.append((task, task_deletion_queue.add_async(task)))
            task_deletion_rpcs = retries

        if output_format == ecore_constants.JSON_OUTPUT:
            response_data = []
            response_total_rows = 0
            for lst_item in query_results:
                response_data.extend(lst_item)
                response_total_rows += len(lst_item)

            response_output = {
                'data': response_data,
                'meta': {
                    'response': {
                        'totalRows': response_total_rows,
                        #'totalSize': '%i kB' % round(len(response_data) / 1024.0),
                        'schema': None,
                        'startDate': effective_start_date.strftime('%Y-%m-%dT00:00:00'),
                        'endDate': effective_end_date.strftime('%Y-%m-%dT00:00:00'),
                        'loadTime': '%i ms' % round((time.time() - start_time) * 1000)
                    },
                    'request': {
                        'namingConvention': naming_convention,
                        'apiAccessKey': api_access_key,
                        'startDate': start_date.strftime('%Y-%m-%dT00:00:00'),
                        'endDate': end_date.strftime('%Y-%m-%dT00:00:00'),
                        'outputFormat': output_format,
                        'clientIp': command.client_ip
                    }
                }
            }

            logging.debug('len(response_data): %i' % len(response_data))
            logging.debug('response_output: %s' % str(response_output)[:500])
            return command.set_response_data(generate_json_output(response_output, preview=preview, attachment=attachment))

        elif output_format == ecore_constants.CSV_OUTPUT:
            try:
                logging.debug('ex_entry_label: %rX\nX' % query_results[1][0])
                logging.debug('ex_entry_label: %sX\nX' % query_results[1][0])
            except IndexError as e:
                logging.error('ex_entry_label: %s' % e.message)

            response_data = ''.join([''.join(e) for e in query_results])
            logging.debug('len(response_data): %i' % (len(response_data) - 1))
            logging.debug('response_data: %s' % response_data[:500])
            return command.set_response_data(generate_csv_output(response_data, attachment=attachment))

        elif True:
            raise AssertionError()

        if preview:
            pass  # TODO(mats.blomdahl@gmail.com): Fix'it!
            # query = '%s LIMIT 4' % query

    def alt_query_subscription(self, command, api_version=None, api_access_key=None, start_date=None, end_date=None,
                        naming_convention='standard', output_format=ecore_constants.JSON_OUTPUT, preview=False,
                        attachment=False, filename=None, dry_run=False):

        def v1_query_subscription(command, start_date, end_date, allowed_fields, output_format, naming_convention, attachment, filename, preview, dry_run, request_meta_data):

            output_version = get_output_version(output_format, naming_convention)

            #disabled_fields = ['real_estate_agent', 'real_estate_company']
            disabled_fields = []
            allowed_fields = []
            for field in content_restrictions.fields:
                if field not in disabled_fields:
                    allowed_fields.append(field)

            query = transaction_model.TransactionCompositeEntry.bq_objects.filter(contract_date__gte=start_date, contract_date__lt=end_date).values(*allowed_fields)
            query = query.version(ecore_constants.BQ_JSON).order_by('contract_date')
            query = query.set_output_format(output_format=output_format, output_version=output_version)
            if preview:
                query = query.limit(5)

            job = query.run()
            query_result = job.get_result()

            if output_format == ecore_constants.JSON_OUTPUT:
                return command.set_response_data(v1_bundle_data_json(query_result, preview, attachment, filename, request_meta_data))
            elif output_format == ecore_constants.CSV_OUTPUT:
                return command.set_response_data(v1_bundle_data_csv(query_result, preview, attachment, filename, request_meta_data))
            else:
                return command.set_response_data({
                    'status_code': ecore_constants.HTTP_BAD_REQUEST.get_status_code(),
                    'errors': {
                        'INVALID OUTPUT FORMAT': 'Malformed/invalid \'outputFormat\' argument. Valid options include \'json\' and \'csv\'.',
                    }
                })

        def get_output_version(output_format, naming_convention):
            output_version = None
            if output_format == ecore_constants.JSON_OUTPUT:
                if naming_convention == 'legacy':
                    output_version = ecore_constants.LEGACY_JSON
                else:
                    output_version = ecore_constants.DEFAULT_JSON
            else: #imlicit CSV_OUTPUT
                if naming_convention == 'legacy':
                    output_version = ecore_constants.LEGACY_CSV
                else:
                    output_version = ecore_constants.DEFAULT_CSV
            return output_version

        def generate_default_filename():
            return '%s_%s-%s_api_output' % (dt_today.strftime('%Y%m%dT%H%M%S'), effective_start_date.strftime('%y%m%d'), effective_end_date.strftime('%y%m%d'))

        def v1_bundle_data_json(data, preview, attachment, filename, request_meta_data):

            response_total_rows = len(data)

            load_time = None
            if 'start_time' in request_meta_data:
                load_time = round((time.time() - request_meta_data['start_time']) * 1000)

            response_wrapper = {
                'success': True,
                'headers': {
                    'Content-Type': 'application/json'
                },
                'data': None,
                'meta': None,
            }

            meta = {
                'response': {
                    'totalRows': response_total_rows,
                    #'totalSize': '%i kB' % round(len(data) / 1024.0),
                    'schema': None,
                    'startDate': request_meta_data.get('effective_start_date',''),
                    'endDate': request_meta_data.get('effective_end_date',''),
                    'loadTime': '%i ms' % load_time,
                },
                'request': {
                    'namingConvention': request_meta_data.get('naming_convention',''),
                    'apiAccessKey': request_meta_data.get('api_access_key',''),
                    'startDate': request_meta_data.get('start_date',''),
                    'endDate': request_meta_data.get('end_date',''),
                    'outputFormat': request_meta_data.get('output_format',''),
                    'clientIp': request_meta_data.get('client_ip',''),
                }
            }

            if attachment:
                # Serve response as attachment
                if filename is None:
                    filename = generate_default_filename()
                response_wrapper['headers']['Content-Disposition'] = 'attachment; filename=%s.json' % filename
                response_wrapper['data'] = data
                repsonse_wrapper['meta'] = meta

            elif preview:
                # Pretty-print plain text response
                response_wrapper['headers']['Content-Type'] = 'text/plain'
                response_wrapper['data'] = json.dumps(data, sort_keys=True, indent=4)

            else:
                response_wrapper['data'] = data
                response_wrapper['meta'] = meta

            logging.debug('len(data): %i' % len(data))
            logging.debug('response_wrapper: %s' % str(response_wrapper)[:500])

            return response_wrapper

        def v1_bundle_data_csv(data, preview, attachment, filename, request_meta_data):

            logging.info('len(response_data): %i' % (len(data) - 1))
            logging.info('response_data: %s' % data[:6])

            response_wrapper = {
                'success': True,
                'headers': {
                    'Content-Type': 'text/csv'
                },
                'data': data,
            }

            if attachment:
                # Serve response as attachment
                if filename is None:
                    filename = generate_default_filename()
                response_wrapper['headers']['Content-Disposition'] = 'attachment; filename=%s.csv' % filename

            return response_wrapper

        def authorize(command, api_access_key):
            errors = {}
            subscription_entry = None

            if ecore_utils.assert_presence_and_basestring(api_access_key):
                try:
                    subscription_entry, user_account_entry, \
                        futures = self._resolve_api_access_key(api_access_key.strip(), delegate_future_mgnt=True)
                    command.add_futures(futures)

                except (ecore_user_model_errors.ApiAccessKeyInvalidError, user_model_errors.SubscriptionNotFoundError,
                        ecore_user_model_errors.UserNotFoundError) as e:
                    logging.error("%s %s" % (type(e), e))
                    errors = {'INVALID API ACCESS KEY': 'Malformed/invalid \'apiAccessKey\' argument (%r).' % api_access_key}

                except ecore_user_model_errors.UserSuspendedError as e:
                    errors = {'USER ACCOUNT SUSPENDED': 'Cannot process query due to suspension. %s' % e.message}

                except ecore_user_model_errors.UserDeletedError as e:
                    errors = {'USER ACCOUNT DELETED': 'Cannot process query due to deletion. %s' % e.message}

                except ecore_user_model_errors.ParentAccountDeletedError as e:
                    errors = {'PARENT ACCOUNT DELETED': 'Cannot process query due to deletion. %s' % e.message}

                except ecore_user_model_errors.ParentAccountSuspendedError as e:
                    errors = {'PARENT ACCOUNT SUSPENDED': 'Cannot process query due to suspension. %s' % e.message}

                except user_model_errors.SubscriptionSuspendedError as e:
                    errors = {'SUBSCRIPTION SUSPENDED': 'Cannot process query due to suspension. %s' % e.message}

                except user_model_errors.SubscriptionDeletedError as e:
                    errors = {'SUBSCRIPTION DELETED': 'Cannot process query due to deletion. %s' % e.message}
            else:
                errors = {'API ACCESS KEY MISSING': 'Required \'apiAccessKey\' argument could not be resolved.'}

            return subscription_entry, errors

        def validate_date_interval(start_date, end_date, today):
            errors = {}
            clean_start_date = None
            clean_end_date = None

            if ecore_utils.assert_presence_and_basestring(start_date):
                try:
                    clean_start_date = formatting.coerce_to_date(start_date)
                except ValueError:
                    errors = {'START DATE MALFORMED': 'Invalid/malformed \'start_date\' argument (%r). The system will only accept ISO date format, e.g. \'2011-10-17\'.' % start_date}
            else:
                errors = {'START DATE MISSING': 'Required \'start_date\' argument could not be resolved.'}

            if clean_start_date:
                if ecore_utils.assert_presence_and_basestring(end_date):
                    try:
                        end_date = formatting.coerce_to_date(end_date)
                        if (end_date - clean_start_date).days < 1:
                            errors = {
                                'ILLEGAL DATE INTERVAL': 'Illegal date interval (%s--%s). Queries must span at least 1 day (you queried for %i days). '
                                        'For maximum performance and reliability, querying for 14-day intervals is highly recommended.' % (clean_start_date.isoformat(), end_date.isoformat(), (end_date - clean_start_date).days)
                            }
                        elif end_date > today:
                            errors = {
                                'ILLEGAL END DATE': 'Illegal \'endDate\' argument (%r). Transactions future from today (i.e. %s) cannot be queried (what date is considered \'today\' updates 04:00 UTC every morning, '
                                    'next update in %s).' % (end_date.isoformat(), today.isoformat(), formatting.timedelta_strftime(dt_breakpoint - dt_today, output_format='%(hours)ih %(minutes)im'))
                                }
                        else:
                            clean_end_date = end_date

                    except ValueError as e:
                            errors = {
                                'END DATE MALFORMED': 'Invalid/malformed \'endDate\' argument (%r). The system will only accept ISO date format, e.g. \'2012-01-17\'.' % end_date
                            }
                else:
                    errors = {'END DATE MISSING': 'Required \'end_Date\' argument could not be resolved.'}

            return clean_start_date, clean_end_date, errors

        def validate_output_format(output_format):
            errors = {}
            clean_output_format = None

            if ecore_utils.assert_presence_and_basestring(output_format):
                output_format = output_format.lower().strip()
                if output_format in [ecore_constants.JSON_OUTPUT, ecore_constants.CSV_OUTPUT]:
                    clean_output_format = output_format
                    errors = {'INVALID OUTPUT FORMAT': 'Malformed/invalid \'outputFormat\' argument. Valid options include \'json\' and \'csv\'.'}
            else:
                errors = {'OUTPUT FORMAT MISSING': 'Valid options include \'json\' and \'csv\'.'}

            return clean_output_format, errors

        def validate_naming_convention(naming_convention):
            clean_naming_convention = None
            errors = {}

            if ecore_utils.assert_presence_and_basestring(naming_convention):
                naming_convention = naming_convention.strip().lower()

                if naming_convention in ['legacy', 'standard', 'default']:
                    clean_naming_convention = naming_convention
                else:
                    errors = {'INVALID NAMING CONVENTION': 'The supplied \'namingConvention\' parameter could not be processed, valid options are \'default\', \'standard\' and \'legacy\'.'}

            return clean_naming_convention, errors

        def validate_optional_boolean(var):
            if var == True or var == 'true':
                return True
            return False

        def check_date_interval_restriction(start_date, end_date, content_restrictions):
            errors = {}

            if start_date < content_restrictions.start_date:
                errors = {'ILLEGAL START DATE': 'Illegal \'start_date\' argument (%r). The subscription\'s lower query limit is set to %s.' % (start_date.isoformat(), content_restrictions.start_date.isoformat())}

            elif content_restrictions.end_date and end_date > content_restrictions.end_date:
                errors = {'ILLEGAL END DATE': 'Illegal \'end_date\' argument (%r). The subscription\'s upper query limit is set to %s.' % (end_date.isoformat(), content_restrictions.end_date.isoformat())}

            elif (end_date - start_date).days > 32:
                errors = {'SERVER ERROR': 'Date range limit exceeded. The number of days queried by a single API call must not exceed 32 (your query covered %i days). '
                            'For maximum performance and reliability, querying for 14-day intervals is highly recommended.' % (end_date - start_date).days}

            return errors

        def validate_api_version(api_version):
            errors = {}
            clean_api_version = None

            if api_version is None:
                errors = {'API VERSION MISSING': 'Required \'apiVersion\' argument could not be resolved.'}
            else:
                try:
                    clean_api_version = int(api_version)
                except ValueError:
                    errors = {'INVALID API VERSION': 'Malformed \'apiVersion\' argument (%r). The only available version (and valid input) is currently \'1\'.' % api_version}

            return clean_api_version, errors

        request_meta_data = {
            'start_time': time.time(),
        }

        # AUTHORISATION
        subscription_entry, errors = authorize(command, api_access_key)
        if not subscription_entry:
            return command.set_response_data({
                'status_code': ecore_constants.HTTP_FORBIDDEN.get_status_code(),
                'errors': errors,
            })

        # DEFINE "TODAY"
        today = datetime.date.today()
        dt_today = datetime.datetime.today()
        dt_breakpoint = datetime.datetime(dt_today.year, dt_today.month, dt_today.day, 4, 0, 0)

        if dt_today <= dt_breakpoint:
            today -= datetime.timedelta(days=1)
        else:
            dt_breakpoint += datetime.timedelta(days=1)

        # VALIDATION
        api_version, errors = validate_api_version(api_version)
        if not api_version:
            return command.set_response_data({
                'status_code': ecore_constants.HTTP_BAD_REQUEST.get_status_code(),
                'errors': errors,
            })

        start_date, end_date, errors = validate_date_interval(start_date, end_date, today)
        if not (start_date and end_date):
            return command.set_response_data({
                'status_code': ecore_constants.HTTP_BAD_REQUEST.get_status_code(),
                'errors': errors,
            })

        output_format, errors = validate_output_format(output_format)
        if not output_format:
            return command.set_response_data({
                'status_code': ecore_constants.HTTP_BAD_REQUEST.get_status_code(),
                'errors': errors,
            })

        naming_convention, errors = validate_naming_convention(naming_convention)
        if not naming_convention:
            return command.set_response_data({
                'status_code': ecore_constants.HTTP_BAD_REQUEST.get_status_code(),
                'errors': errors,
            })

        preview = validate_optional_boolean(preview)
        attachment = validate_optional_boolean(attachment)
        dry_run = validate_optional_boolean(dry_run)
        # TODO: validate filename

        # RESTRICTIONS CHECK
        content_restrictions = subscription_entry.content_restrictions
        errors = check_date_interval_restriction(start_date, end_date, content_restrictions)
        if errors:
            return command.set_response_data({
                'status_code': ecore_constants.HTTP_FORBIDDEN.get_status_code(),
                'errors': errors,
            })
        # Restrict to subscription ´update_frequency´ config
        effective_start_date, effective_end_date = subscription_entry.restrict_effective_query_date_range(start_date, end_date)
        effective_start_date = datetime.datetime(year=effective_start_date.year, month=effective_start_date.month, day=effective_start_date.day)
        effective_end_date = datetime.datetime(year=effective_end_date.year, month=effective_end_date.month, day=effective_end_date.day)

        logging.debug('effective date interval: %s--%s' % (effective_start_date, effective_end_date))

        # COLLECT GENERAL DATA
        #allowed_output_fields = content_restrictions.to_dict()['fields']
        allowed_fields = content_restrictions.fields
        #parent_organization_entry_future = subscription_entry.key.parent().get_async(use_memcache=True)

        request_meta_data.update({
            'api_access_key': api_access_key,
            'start_date': start_date.strftime('%Y-%m-%dT00:00:00'),
            'end_date': end_date.strftime('%Y-%m-%dT00:00:00'),
            'effective_start_date': effective_start_date.strftime('%Y-%m-%dT00:00:00'),
            'effective_end_date': effective_end_date.strftime('%Y-%m-%dT00:00:00'),
            'output_format': output_format,
            'naming_convention': naming_convention,
            'client_ip': command.client_ip,
        })

        logging.info('Dispatching query subscription with request meta data: %s' % pprint.pformat(request_meta_data))

        # DISPATCH / ROUTE
        if api_version == 1:
            return v1_query_subscription(command, effective_start_date, effective_end_date, allowed_fields, output_format, naming_convention, attachment, filename, preview, dry_run, request_meta_data)
        else:
            return command.set_response_data({
                'status_code': ecore_constants.HTTP_NOT_IMPLEMENTED.get_status_code(),
                'errors': {'INVALID API VERSION': 'Invalid \'apiVersion\' argument (%r). The only available version (and valid input) is currently \'1\'.' % api_version}
            })

    def chart_data(self, command, kind=None, offset=None, page_size=50, page=None, order_by='sys_modified',
                   order_direction='ASC', filter_by=None, group_by=None):
        u"""Queries the Datastore to produce chart data for Ext JS `Chart` components.

        Args:
            command: A ``Command`` RPC wrapper.
            kind: Name of the target Datastore kind.
            page_size: Number of records per page to return.
            offset: Offset argument for the NDB query.
            page: Target page in the query result (note that ``offset`` and ``page`` are mutually redundant).
            order_by: Target sort property for the NDB query.
            order_direction: Sort direction for ``order_by``.
            filter_by: Dummy filter property for the NDB query.
            group_by: Result grouping (not implemented).

        Returns:
            The RPC wrapper, configured with the handler's response.
        """

        if not command.session_valid_and_authenticated():
            return command.set_response_data({'status_code': ecore_constants.HTTP_FORBIDDEN.get_status_code(),
                                              'errors': {'SESSION INVALID': _SESSION_INVALID_ERROR_MSG}})
        try:
            kind_cls = self._resolve_datastore_kind(kind)
        except handler_errors.AccessDeniedError as e:
            return command.set_response_data({'status_code': ecore_constants.HTTP_ACCESS_DENIED.get_status_code(),
                                              'errors': {'ACCESS DENIED': e.message}})
        except handler_errors.BadRequestError as e:
            return command.set_response_data({'status_code': ecore_constants.HTTP_BAD_REQUEST.get_status_code(),
                                              'errors': {'BAD REQUEST': e.message}})

        filter_by = [{'property': 'ancestor', 'value': command.user_account_entry.key.parent().urlsafe()}]
        return command.set_response_data(
            self._datastore_subset(kind_cls, page, page_size, offset, order_by, order_direction, filter_by, group_by))

    def grid_data(self, command, kind=None, offset=None, page_size=50, page=None, order_by='sys_modified',
                  order_direction='ASC', filter_by=None, group_by=None):
        u"""Queries the Datastore to produce grid data for the Ext JS `GridView` component.

        Args:
            command: A ``Command`` RPC wrapper.
            kind: Name of the target Datastore kind.
            page_size: Number of records per page to return.
            offset: Offset argument for the NDB query.
            page: Target page in the query result (note that ``offset`` and ``page`` are mutually redundant).
            order_by: Target sort property for the NDB query.
            order_direction: Sort direction for ``order_by``.
            filter_by: Dummy filter property for the NDB query.
            group_by: Result grouping (not implemented).

        Returns:
            The RPC wrapper, configured with the handler's response.
        """

        if not command.session_valid_and_authenticated():
            return command.set_response_data({'status_code': ecore_constants.HTTP_FORBIDDEN.get_status_code(),
                                              'errors': {'SESSION INVALID': _SESSION_INVALID_ERROR_MSG}})
        try:
            kind_cls = self._resolve_datastore_kind(kind)
        except handler_errors.AccessDeniedError as e:
            return command.set_response_data({'status_code': ecore_constants.HTTP_ACCESS_DENIED.get_status_code(),
                                              'errors': {'ACCESS DENIED': e.message}})
        except handler_errors.BadRequestError as e:
            return command.set_response_data({'status_code': ecore_constants.HTTP_BAD_REQUEST.get_status_code(),
                                              'errors': {'BAD REQUEST': e.message}})

        filter_by = [{'property': 'ancestor', 'value': command.user_account_entry.key.parent().urlsafe()}]

        return command.set_response_data(
            self._datastore_subset(kind_cls, page, page_size, offset, order_by, order_direction, filter_by, group_by))

    def daily_stats(self, command, kind=None, offset=None, page_size=None, page=None, order_by='contract_date',
                    order_direction='ASC', filter_by=None, group_by=None):
        u"""Queries the Datastore for `system stats` entities over the last 30 days to produce chart data for the
            Ext JS `Chart` components on the subscriber UI `System Performance` tabpanel.

        Args:
            command: A ``Command`` RPC wrapper.
            kind: Not implemented.
            page_size: Not implemented.
            offset: Not implemented.
            page: Not implemented.
            order_by: Not implemented.
            order_direction: Not implemented.
            filter_by: Not implemented.
            group_by: Not implemented.

        Returns:
            The RPC wrapper, configured with the handler's response.
        """

        dt_today = datetime.datetime.today()
        stats_kinds_prefixed = {'lf': transaction_model.LfRawDataStats,
                                'sf': transaction_model.SfRawDataStats,
                                'sfd': transaction_model.SfdRawDataStats,
                                'capitex': transaction_model.CapitexRawDataStats,
                                'scb': transaction_model.ScbRawDataStats,
                                'tce': transaction_model.TransactionCompositeStats}

        target_dates = formatting.date_range_sub_intervals(formatting.dt_monthdelta(dt_today, -1), dt_today, 1,
                                                           output_date_type=datetime.date)
        start_date, end_date = target_dates[0][0], target_dates[-1][1]
        daily_stats = dict.fromkeys([interval[0].isoformat() for interval in target_dates], None)

        index = 0

        for day, stats in daily_stats.iteritems():
            day_dt_isoformat = formatting.coerce_to_dt(day).isoformat()
            daily_stats[day] = {
                'stats_datetime': day_dt_isoformat,

                'datastore_key_urlsafe': 'urlsafe_dummy_%i_%s' % (index, day_dt_isoformat),
                'datastore_key_id': 'id_dummy_%i_%s' % (index, day_dt_isoformat),

                'lf_created_entry_count': 0,
                'lf_updated_entry_count': 0,
                'sf_created_entry_count': 0,
                'sf_updated_entry_count': 0,
                'sfd_created_entry_count': 0,
                'sfd_updated_entry_count': 0,
                'scb_created_entry_count': 0,
                'scb_updated_entry_count': 0,
                'capitex_created_entry_count': 0,
                'capitex_updated_entry_count': 0,
                'mspecs_created_entry_count': 0,
                'mspecs_updated_entry_count': 0,
                'tce_created_entry_count': 0,
                'tce_updated_entry_count': 0,

                'tce_mstat_sys_sf_created_entry_count': 0,
                'tce_mstat_sys_lf_created_entry_count': 0,
                'tce_mstat_sys_capitex_created_entry_count': 0,
                'tce_mstat_sys_sfd_created_entry_count': 0,
                'tce_mstat_sys_scb_created_entry_count': 0,
                'tce_mstat_sys_mspecs_created_entry_count': 0,

                'sys_modified': day_dt_isoformat,
                'sys_created': day_dt_isoformat
            }
            index += 1

        stats_futures = []
        for prefix, kind_cls in stats_kinds_prefixed.iteritems():
            stats_futures.append((prefix,
                                  kind_cls.query(
                                      ndb.AND(kind_cls.stats_calculation_date >= start_date,
                                              kind_cls.stats_calculation_date < end_date)).fetch_async(
                                                  len(target_dates), batch_size=len(target_dates), deadline=3600)))

        for prefix, future in stats_futures:
            results = future.get_result()
            for entry in results:
                day_key = entry.stats_calculation_date.isoformat()
                mod_key = '%s_updated_entry_count' % prefix
                added_key = '%s_created_entry_count' % prefix

                if prefix == 'tce':
                    tce_sf_created_key = 'tce_mstat_sys_sf_created_entry_count'
                    if daily_stats[day_key][tce_sf_created_key] >= 0:
                        daily_stats[day_key][tce_sf_created_key] += entry.mstat_sys_sf_created_entry_count
                        if daily_stats[day_key][tce_sf_created_key] >= 2**14:
                            daily_stats[day_key][tce_sf_created_key] = -1

                    tce_lf_created_key = 'tce_mstat_sys_lf_created_entry_count'
                    if daily_stats[day_key][tce_lf_created_key] >= 0:
                        daily_stats[day_key][tce_lf_created_key] += entry.mstat_sys_lf_created_entry_count
                        if daily_stats[day_key][tce_lf_created_key] >= 2**14:
                            daily_stats[day_key][tce_lf_created_key] = -1

                    tce_capitex_created_key = 'tce_mstat_sys_capitex_created_entry_count'
                    if daily_stats[day_key][tce_capitex_created_key] >= 0:
                        daily_stats[day_key][tce_capitex_created_key] += entry.mstat_sys_capitex_created_entry_count
                        if daily_stats[day_key][tce_capitex_created_key] >= 2**14:
                            daily_stats[day_key][tce_capitex_created_key] = -1

                    tce_sfd_created_key = 'tce_mstat_sys_sfd_created_entry_count'
                    if daily_stats[day_key][tce_sfd_created_key] >= 0:
                        daily_stats[day_key][tce_sfd_created_key] += entry.mstat_sys_sfd_created_entry_count
                        if daily_stats[day_key][tce_sfd_created_key] >= 2**14:
                            daily_stats[day_key][tce_sfd_created_key] = -1

                    tce_scb_created_key = 'tce_mstat_sys_scb_created_entry_count'
                    if daily_stats[day_key][tce_scb_created_key] >= 0:
                        daily_stats[day_key][tce_scb_created_key] += entry.mstat_sys_scb_created_entry_count
                        if daily_stats[day_key][tce_scb_created_key] >= 2**14:
                            daily_stats[day_key][tce_scb_created_key] = -1

                    tce_mspecs_created_key = 'tce_mstat_sys_mspecs_created_entry_count'
                    if daily_stats[day_key][tce_mspecs_created_key] >= 0:
                        daily_stats[day_key][tce_mspecs_created_key] += entry.mstat_sys_mspecs_created_entry_count
                        if daily_stats[day_key][tce_mspecs_created_key] >= 2**14:
                            daily_stats[day_key][tce_mspecs_created_key] = -1

                if daily_stats[day_key][mod_key] >= 0:
                    daily_stats[day_key][mod_key] += entry.updated_entry_count
                    if daily_stats[day_key][mod_key] >= 2**14:
                        daily_stats[day_key][mod_key] = -1

                if daily_stats[day_key][added_key] >= 0:
                    daily_stats[day_key][added_key] += entry.mstat_sys_created_count
                    if daily_stats[day_key][added_key] >= 2**14:
                        daily_stats[day_key][added_key] = -1

        output = sorted(daily_stats.values(), key=operator.itemgetter('stats_datetime'))

        return command.set_response_data({'success': True,
                                          'data': {'success': True, 'data': output, 'total': len(output)}})
