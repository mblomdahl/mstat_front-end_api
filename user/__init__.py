# -*- coding: utf-8 -*-
#
# Title:   mstat_front-end_api.user
# Author:  Mats Blomdahl
# Version: 2012-08-21

u"""mstat_front-end_api.user module

Usage:
    Invoke handlers deployed on the ``/user/api`` as REST or Ext Direct endpoints.
"""

from api_interface import ApiInterface
